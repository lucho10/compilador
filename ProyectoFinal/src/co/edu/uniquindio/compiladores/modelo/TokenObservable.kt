package co.edu.uniquindio.compiladores.modelo

import co.edu.uniquindio.compiladores.lexico.Token
import javafx.beans.property.SimpleStringProperty
import javafx.beans.property.StringProperty

class TokenObservable(token: Token) {

    var lexema: StringProperty = SimpleStringProperty(token.lexema)
    var categoria: StringProperty = SimpleStringProperty(token.categoria.name)
    var fila: StringProperty = SimpleStringProperty(token.fila.toString())
    var columna: StringProperty = SimpleStringProperty(token.fila.toString())

}