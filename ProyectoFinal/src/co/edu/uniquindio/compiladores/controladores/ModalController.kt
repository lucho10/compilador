package co.edu.uniquindio.compiladores.controladores

import co.edu.uniquindio.compiladores.lexico.Error
import javafx.collections.FXCollections
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.TableColumn
import javafx.scene.control.TableView
import javafx.scene.control.cell.PropertyValueFactory
import java.net.URL
import java.util.*
import kotlin.collections.ArrayList

class ModalController: Initializable {
    @FXML
    lateinit var colM: TableColumn<Error, String>

    @FXML
    lateinit var colF: TableColumn<Error, Int>

    @FXML
    lateinit var colC: TableColumn<Error, Int>

    @FXML
    lateinit var table: TableView<Error>


    override fun initialize(location: URL?, resources: ResourceBundle?) {
        colM.cellValueFactory = PropertyValueFactory("error")
        colF.cellValueFactory = PropertyValueFactory("fila")
        colC.cellValueFactory = PropertyValueFactory("column")
    }

    fun init(errors: ArrayList<Error>){
        table.items = FXCollections.observableArrayList(errors)
    }
}