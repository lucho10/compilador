package co.edu.uniquindio.compiladores.controladores

import co.edu.uniquindio.compiladores.app.Aplicacion
import co.edu.uniquindio.compiladores.lexico.AnalizadorLexico
import co.edu.uniquindio.compiladores.lexico.Error
import co.edu.uniquindio.compiladores.lexico.Token
import co.edu.uniquindio.compiladores.semantica.AnalizadorSemantico
import co.edu.uniquindio.compiladores.sintaxis.AnalizadorSintactico
import co.edu.uniquindio.compiladores.sintaxis.UnidadDeCompilacion
import javafx.application.Platform
import javafx.collections.FXCollections
import javafx.event.ActionEvent
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.fxml.Initializable
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.control.*
import javafx.scene.control.cell.PropertyValueFactory
import javafx.scene.image.ImageView
import javafx.scene.layout.Pane
import javafx.stage.Stage
import java.awt.Desktop
import java.io.File
import java.net.URI
import java.net.URL
import java.util.*
import kotlin.collections.ArrayList


class PrincipalController: Initializable {

    private lateinit var uc: UnidadDeCompilacion;
    private lateinit var lexico:AnalizadorLexico;
    private lateinit var semantica:AnalizadorSemantico;
    private lateinit var sintaxis: AnalizadorSintactico

    @FXML
    lateinit var arbolVisual: TreeView<String>

    @FXML
    lateinit var  imgFelipe: ImageView

    @FXML
    lateinit var imagenLuis: ImageView

    @FXML
    lateinit var imagenAlejandra: ImageView

    @FXML
    lateinit var txtSimulacion: TextArea

    @FXML
    lateinit var txtEntradaAnalisis: TextArea

    @FXML
    lateinit var tablaSimualcion: TableView<Token>

    @FXML
    lateinit var tablaAnalisis: TableView<Token>

    @FXML
    lateinit var panelPrueba: Pane

    @FXML
    lateinit var panelSimulacion: Pane

    @FXML
    lateinit var panelInformacion: Pane

    @FXML
    lateinit var  imgPrueba: ImageView

    @FXML
    lateinit var imgSimulacion: ImageView

    @FXML
    lateinit var imgInformacion: ImageView


    @FXML
    lateinit var colLexemaS: TableColumn<Token, String>

    @FXML
    lateinit var colCategoriaS: TableColumn<Token, String>

    @FXML
    lateinit var colFilaS: TableColumn<Token, Int>

    @FXML
    lateinit var colColS: TableColumn<Token, Int>

    @FXML
    lateinit var colLexema: TableColumn<Token, String>

    @FXML
    lateinit var colCategoria: TableColumn<Token, String>

    @FXML
    lateinit var colFila: TableColumn<Token, Int>

    @FXML
    lateinit var colColumna: TableColumn<Token, Int>

    @FXML
    private val link: Hyperlink? = null

    override fun initialize(location: URL?, resources: ResourceBundle?) {


        colLexema.cellValueFactory = PropertyValueFactory("lexema")
        colCategoria.cellValueFactory = PropertyValueFactory("categoria")
        colFila.cellValueFactory = PropertyValueFactory("fila")
        colColumna.cellValueFactory = PropertyValueFactory("columna")


        colLexemaS.cellValueFactory = PropertyValueFactory("lexema")
        colCategoriaS.cellValueFactory = PropertyValueFactory("categoria")
        colFilaS.cellValueFactory = PropertyValueFactory("fila")
        colColS.cellValueFactory = PropertyValueFactory("columna")


    }

    @FXML
    fun onLink(event: ActionEvent?) {
        print("link")
        val d: Desktop = Desktop.getDesktop()
        d.browse(URI("https://gitlab.com/lucho10/compilador"))
    }


    @FXML
    fun mostrarPanelInformacion(e: ActionEvent) {

         panelInformacion.toFront();
    }

    @FXML
    fun mostrarPanelPrueba(e: ActionEvent) {

        panelPrueba.toFront();
    }

    @FXML
    fun mostrarPanelSimulacion(e: ActionEvent) {

        panelSimulacion.toFront();

    }


    @FXML
    fun cerrarAplicacion(event: ActionEvent?) {

      Platform.exit()


    }

    @FXML
    fun traducir(event: ActionEvent?) {

        //TODO lexico.listaErrores.isEmpty() && sintaxis.listaErrores.isEmpty() && semantica.listaErrores.isEmpty()

        if(uc!=null && lexico.errores.isEmpty() && sintaxis.listaErrores.isEmpty() && semantica.listaErrores.isEmpty()) {

            val codigo = uc.getJavaCode()
            println(codigo)
            /**
             *  File("src/Principal.java").writeText(codigo)

            try {
            val p = Runtime.getRuntime().exec("javac src/Principal.java")
            p.waitFor()
            Runtime.getRuntime().exec("java principal",null,File("src"))
            }catch (ea: Exception){
            ea.printStackTrace()
            }

            }else{
            val alert = Alert(Alert.AlertType.ERROR)
            alert.headerText = null
            alert.contentText = "El codigo no se puede traducir por que hay errores"
            alert.show()
            }
             */
        }


    }

    @FXML
    fun iniciarSimulacion(event: ActionEvent?) {
        val entradaSimulacion ="n2!hola!!!cadena!!orloop1>>=|_metodo que tin_| += +++ and 7.8d @nodejs nose"
        txtSimulacion.text = entradaSimulacion
        var lexico = AnalizadorLexico(entradaSimulacion)
        lexico.analizar();
        tablaSimualcion.items = FXCollections.observableArrayList(lexico.tokens)
    }


    fun iniciarAnalisis(actionEvent: ActionEvent) {

        if (txtEntradaAnalisis.length > 0) {

            lexico = AnalizadorLexico(txtEntradaAnalisis.text)
            lexico.analizar();
            tablaAnalisis.items = FXCollections.observableArrayList(lexico.tokens)
            if(lexico.errores.isEmpty()){
                sintaxis = AnalizadorSintactico(lexico.tokens)
                uc = sintaxis.esUnidadDeCompilacion()!!
                println("errores sintacticos" + sintaxis.listaErrores)
                if(sintaxis.listaErrores.size > 0){
                    showErrors("Errores Sintacticos",sintaxis.listaErrores)
                }

                if(uc!= null){
                    arbolVisual.root = uc.getArbolVisual()
                    semantica = AnalizadorSemantico(uc)
                    semantica.llenarTablaSimbolos()
                    semantica.analizarSemantica()
                    println(semantica.tablaSimbolos)
                    if(semantica.listaErrores.size > 0){
                        showErrors("Errores Semanticos", semantica.listaErrores)
                    }
                }
            } else {
                showErrors("Errores Lexicos",lexico.errores)
            }



        }


    }

    fun showErrors(name: String, errors: ArrayList<Error>){
        val loader = FXMLLoader(Aplicacion::class.java.getResource("/modal.fxml"))
        val parent: Parent = loader.load()
        val controller = loader.getController<ModalController>()
        controller.init(errors)
        val scene = Scene( parent )
        val stage = Stage()
        stage.scene = scene
        stage.title = name
        stage.show()
    }

    @FXML
    fun reiniciarAnalisis(event: ActionEvent?) {

        txtEntradaAnalisis.clear()

    }
}
