package co.edu.uniquindio.compiladores.semantica
import co.edu.uniquindio.compiladores.lexico.Error

class TablaSimbolos (var listaErrores: ArrayList<Error>){


    var listaSimbolos: ArrayList<Simbolo> = ArrayList()

    /**
     * permite guardar un simbolo que representa una variable una constante, un parametro, arreglo
     */

    fun guardarSimboloValor(nombre: String, tipoDato:String, modificable:Boolean, ambito:String, fila:Int, columna: Int){

        var s = buscarSimboloValor(nombre,ambito,fila,columna)

        if(s == null) {
            listaSimbolos.add(Simbolo(nombre, tipoDato, modificable, ambito, fila, columna))
        } else {
            listaErrores.add(Error("El campo ${nombre} ya existe dentro del ambito ${ambito}",fila,columna))

        }
    }

    /**
     * permite guardar un simbolo que representa
     */

    fun guardarSimboloFuncion(nombre: String, tipoRetorno:String, tiposParametros: ArrayList<String>,ambito:String,fila:Int, columna: Int){

          val s = buscarSimboloFuncion(nombre,tiposParametros)

          if (s == null){
              listaSimbolos.add(Simbolo(nombre,tipoRetorno,tiposParametros,ambito))
          } else {
              listaErrores.add(Error("La Funcion ${nombre} ya existe dentro del ambito ${ambito}",fila,columna))
          }


    }

    /**
     * permite buscar un valor en la tabla de simbolos
     */

    fun buscarSimboloValor( nombre:String, ambito: String, fila: Int, columna: Int ):Simbolo?{

          for (s in listaSimbolos ){
              if(s.tiposParametros==null) {
                  if (s.nombre == nombre && s.ambito == ambito) {
                      if(s.fila <= fila ){
                          return s
                      }
                      return null
                  }
              }
          }
        return null
    }

    /**
     * permite buscar una funcion en la tabla de simbolos
     */

    fun buscarSimboloFuncion( nombre:String, tiposParametros: ArrayList<String> ):Simbolo?{
        for (s in listaSimbolos ){

            if(s.tiposParametros!=null){
                if(s.nombre == nombre && s.tiposParametros == tiposParametros){
                    return s
                }
            }

        }
        return null
    }

    override fun toString(): String {
        return "TablaSimbolos(listaSimbolos=$listaSimbolos)"
    }


}