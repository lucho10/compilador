package co.edu.uniquindio.compiladores.prueba

import co.edu.uniquindio.compiladores.lexico.AnalizadorLexico
import co.edu.uniquindio.compiladores.sintaxis.AnalizadorSintactico

fun main() {
    val lexico = AnalizadorLexico("log")
    lexico.analizar()
    println(lexico.tokens)
    val sintaxis = AnalizadorSintactico(lexico.tokens)
    println(sintaxis.esUnidadDeCompilacion())
    println(sintaxis.listaErrores)
}