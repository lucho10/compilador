package co.edu.uniquindio.compiladores.app

import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.stage.Stage
import javafx.stage.StageStyle

class Aplicacion  : Application(){

    override fun start(primaryStage: Stage?) {

        val loader = FXMLLoader(Aplicacion::class.java.getResource("/principal.fxml"))
        val parent: Parent = loader.load()

        val scene = Scene( parent )

        scene.stylesheets.add(Aplicacion::class.java.getResource("/css/table_view.css").toString());
        primaryStage?.initStyle(StageStyle.UNDECORATED)
        primaryStage?.scene = scene
        primaryStage?.title="mi compilador"
        primaryStage?.show()

    }


    companion object{

        @JvmStatic
        fun main(args : Array<String>){
            launch(Aplicacion::class.java)
        }

    }




}