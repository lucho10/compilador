package co.edu.uniquindio.compiladores.lexico

class AnalizadorLexico(var codigoFuente:String) {

    var posicionActual = 0
    var caracterActual = codigoFuente[0]
    var tokens = ArrayList<Token>()
    var errores = ArrayList<Error>()
    var finCodigo = 0.toChar()
    var filaActual = 0
    var columnaActual = 0
    val palabrasReservadas = arrayOf<String>("R","F","loop","log","fx","time","return","else","class","imp","if","lim","dx","Bool","Int","Decimal","String","Char","void","input","[*]") //an array of Strings


    fun saveToken(lexema: String, categoria: Categoria, fila:Int, columna: Int) = tokens.add(Token(lexema,categoria,fila,columna))

    private fun reportarError(mensaje : String, fila: Int,columna: Int){
        errores.add(Error(mensaje, fila, columna ))
    }

    fun analizar(){

        while (caracterActual != finCodigo){
            if(caracterActual == ' ' || caracterActual == '\t' || caracterActual == '\n'){
                obtenerSiguienteCaracter()
                continue
            }
            if(esIdentificador()) continue
            if(esPalabraReservada()) continue
            if(esEntero()) continue
            if(esDecimal()) continue
            if(esOperadorIncrementoDecremento()) continue
            if(esOperadorAritmetico()) continue
            if(esOperadorDeAsignacion()) continue
            if(esOperadorRelacional()) continue
            if(esOperadorLogico()) continue
            if(esCaracter()) continue
            if(esCadena()) continue
            if(esFinSentencia()) continue
            if(esComentarioBloque())continue
            if(esComentarioLinea())continue
            if(esParentesisIzqDer())continue
            if(esLlavesIzqDer())continue
            if(esComa())continue
            if(esDosPuntos())continue
            if(esPunto())continue
            if(esCorcheteIzqDer())continue
            if(esCaracterConcatenar())continue

            saveToken(""+caracterActual,Categoria.DESCONOCIDO,filaActual,columnaActual)
            obtenerSiguienteCaracter()
        }
    }
    fun esCorcheteIzqDer(): Boolean {
        if (caracterActual.equals('[')) {
            saveToken("[", Categoria.CORCHETE_IZQUIERDO, filaActual, columnaActual)
            obtenerSiguienteCaracter()
            return true
        }
        if (caracterActual.equals(']')) {
            saveToken("]", Categoria.CORCHETE_DERECHO, filaActual, columnaActual)
            obtenerSiguienteCaracter()
            return true
        }
        return false
    }
    fun esComa(): Boolean{
        if(caracterActual.equals(',')){
            var lexema = ""
            var filaInicial = filaActual
            var columnaInicial = columnaActual
            lexema+=caracterActual
            obtenerSiguienteCaracter()
            saveToken(lexema, Categoria.COMA, filaInicial, columnaInicial)
            return true
        }
        return false
    }
    fun esPunto(): Boolean{
        if(caracterActual.equals('.')){
            var lexema = ""
            var filaInicial = filaActual
            var columnaInicial = columnaActual
            lexema+=caracterActual
            obtenerSiguienteCaracter()
            saveToken(lexema, Categoria.PUNTO, filaInicial, columnaInicial)
            return true
        }
        return false
    }
    fun esDosPuntos(): Boolean{
        if(caracterActual.equals(':')){
            var lexema = ""
            var filaInicial = filaActual
            var columnaInicial = columnaActual
            lexema+=caracterActual
            obtenerSiguienteCaracter()
            saveToken(lexema, Categoria.DOS_PUNTOS, filaInicial, columnaInicial)
            return true
        }
        return false
    }
    fun devolverse(filaI: Int, colI: Int, tam: Int) {
        filaActual = filaI
        columnaActual = colI
        posicionActual-= tam
        caracterActual = codigoFuente[posicionActual]
    }
    fun esPalabraReservada(): Boolean {
        println("en palabra ")
        var lexema = ""
        var filaInicial = filaActual
        var columnaInicial = columnaActual
        if(caracterActual.equals('R')) {
            lexema += caracterActual
            obtenerSiguienteCaracter()
            saveToken(lexema, Categoria.PALABRA_RESERVADA, filaInicial, columnaInicial)
            return true
        }
        if(caracterActual.equals('F')) {
            lexema += caracterActual
            obtenerSiguienteCaracter()
            saveToken(lexema, Categoria.PALABRA_RESERVADA, filaInicial, columnaInicial)
            return true
        }
        if(caracterActual.equals('l')){
            lexema+=caracterActual
            obtenerSiguienteCaracter()
            if(caracterActual.equals('o')){
                lexema+=caracterActual
                obtenerSiguienteCaracter()
                if(caracterActual.equals('o')) {
                    lexema += caracterActual
                    obtenerSiguienteCaracter()
                    if (caracterActual.equals('p')) {
                        lexema += caracterActual
                        obtenerSiguienteCaracter()
                        saveToken(lexema, Categoria.PALABRA_RESERVADA, filaInicial, columnaInicial)
                        return true
                    }
                    devolverse(filaInicial, columnaInicial, lexema.length)
                    return false
                }
                if(caracterActual.equals('g')) {
                    lexema += caracterActual
                    obtenerSiguienteCaracter()
                    saveToken(lexema, Categoria.PALABRA_RESERVADA, filaInicial, columnaInicial)
                    return true
                }
                devolverse(filaInicial, columnaInicial, lexema.length)
                return false
            }
            devolverse(filaInicial, columnaInicial, lexema.length)
            return false
        }
        if(caracterActual.equals('f')){
            lexema+=caracterActual
            obtenerSiguienteCaracter()
            if(caracterActual.equals('x')) {
                lexema+=caracterActual
                obtenerSiguienteCaracter()
                saveToken(lexema, Categoria.PALABRA_RESERVADA, filaInicial, columnaInicial)
                return true
            }
            devolverse(filaInicial, columnaInicial, lexema.length)
            return false
        }
        if(caracterActual.equals('t')){
            lexema+=caracterActual
            obtenerSiguienteCaracter()
            if(caracterActual.equals('i')){
                lexema+=caracterActual
                obtenerSiguienteCaracter()
                if(caracterActual.equals('m')){
                    lexema+=caracterActual
                    obtenerSiguienteCaracter()
                    if(caracterActual.equals('e')){
                        lexema+=caracterActual
                        obtenerSiguienteCaracter()
                        saveToken(lexema, Categoria.PALABRA_RESERVADA, filaInicial, columnaInicial)
                        return true
                    }
                    devolverse(filaInicial, columnaInicial, lexema.length)
                    return false
                }
                devolverse(filaInicial, columnaInicial, lexema.length)
                return false
            }
            devolverse(filaInicial, columnaInicial, lexema.length)
            return false
        }
        if(caracterActual.equals('v')){
            lexema+=caracterActual
            obtenerSiguienteCaracter()
            if(caracterActual.equals('o')){
                lexema+=caracterActual
                obtenerSiguienteCaracter()
                if(caracterActual.equals('i')){
                    lexema+=caracterActual
                    obtenerSiguienteCaracter()
                    if(caracterActual.equals('d')){
                        lexema+=caracterActual
                        obtenerSiguienteCaracter()
                        saveToken(lexema, Categoria.PALABRA_RESERVADA, filaInicial, columnaInicial)
                        return true
                    }
                    devolverse(filaInicial, columnaInicial, lexema.length)
                    return false
                }
                devolverse(filaInicial, columnaInicial, lexema.length)
                return false
            }
            devolverse(filaInicial, columnaInicial, lexema.length)
            return false
        }
        if(caracterActual.equals('r')) {
            lexema += caracterActual
            obtenerSiguienteCaracter()
            if(caracterActual.equals('e')) {
                lexema += caracterActual
                obtenerSiguienteCaracter()
                if(caracterActual.equals('t')) {
                    lexema += caracterActual
                    obtenerSiguienteCaracter()
                    if(caracterActual.equals('u')) {
                        lexema += caracterActual
                        obtenerSiguienteCaracter()
                        if(caracterActual.equals('r')) {
                            lexema += caracterActual
                            obtenerSiguienteCaracter()
                            if(caracterActual.equals('n')) {
                                lexema += caracterActual
                                obtenerSiguienteCaracter()
                                saveToken(lexema, Categoria.PALABRA_RESERVADA, filaInicial, columnaInicial)
                                return true
                            }
                            devolverse(filaInicial, columnaInicial, lexema.length)
                            return false
                        }
                        devolverse(filaInicial, columnaInicial, lexema.length)
                        return false
                    }
                    devolverse(filaInicial, columnaInicial, lexema.length)
                    return false
                }
                devolverse(filaInicial, columnaInicial, lexema.length)
                return false
            }
            devolverse(filaInicial, columnaInicial, lexema.length)
            return false
        }
        if(caracterActual.equals('e')) {
            lexema += caracterActual
            obtenerSiguienteCaracter()
            if(caracterActual.equals('l')) {
                lexema += caracterActual
                obtenerSiguienteCaracter()
                if(caracterActual.equals('s')) {
                    lexema += caracterActual
                    obtenerSiguienteCaracter()
                    if(caracterActual.equals('e')) {
                        lexema += caracterActual
                        obtenerSiguienteCaracter()
                        saveToken(lexema, Categoria.PALABRA_RESERVADA, filaInicial, columnaInicial)
                        return true
                    }
                    devolverse(filaInicial, columnaInicial, lexema.length)
                    return false
                }
                devolverse(filaInicial, columnaInicial, lexema.length)
                return false
            }
            devolverse(filaInicial, columnaInicial, lexema.length)
            return false
        }
        if(caracterActual.equals('c')) {
            lexema += caracterActual
            obtenerSiguienteCaracter()
            if(caracterActual.equals('l')) {
                lexema += caracterActual
                obtenerSiguienteCaracter()
                if(caracterActual.equals('a')) {
                    lexema += caracterActual
                    obtenerSiguienteCaracter()
                    if(caracterActual.equals('s')) {
                        lexema += caracterActual
                        obtenerSiguienteCaracter()
                        if(caracterActual.equals('s')) {
                            lexema += caracterActual
                            obtenerSiguienteCaracter()
                            saveToken(lexema, Categoria.PALABRA_RESERVADA, filaInicial, columnaInicial)
                            return true
                        }
                        devolverse(filaInicial, columnaInicial, lexema.length)
                        return false
                    }
                    devolverse(filaInicial, columnaInicial, lexema.length)
                    return false
                }
                devolverse(filaInicial, columnaInicial, lexema.length)
                return false
            }
            devolverse(filaInicial, columnaInicial, lexema.length)
            return false
        }
        if(caracterActual.equals('i')) {
            lexema += caracterActual
            obtenerSiguienteCaracter()
            if(caracterActual.equals('m')) {
                lexema += caracterActual
                obtenerSiguienteCaracter()
                if(caracterActual.equals('p')) {
                    lexema += caracterActual
                    obtenerSiguienteCaracter()
                    saveToken(lexema, Categoria.PALABRA_RESERVADA, filaInicial, columnaInicial)
                    return true
                }
                devolverse(filaInicial, columnaInicial, lexema.length)
                return false
            }
            if(caracterActual.equals('f')) {
                lexema += caracterActual
                obtenerSiguienteCaracter()
                saveToken(lexema, Categoria.PALABRA_RESERVADA, filaInicial, columnaInicial)
                return true
            }
            if(caracterActual.equals('n')) {
                    lexema += caracterActual
                    obtenerSiguienteCaracter()
                    if(caracterActual.equals('p')) {
                        lexema += caracterActual
                        obtenerSiguienteCaracter()
                        if(caracterActual.equals('u')) {
                            lexema += caracterActual
                            obtenerSiguienteCaracter()
                            if(caracterActual.equals('t')) {
                                lexema += caracterActual
                                obtenerSiguienteCaracter()
                                saveToken(lexema, Categoria.PALABRA_RESERVADA, filaInicial, columnaInicial)
                                return true
                            }
                            devolverse(filaInicial, columnaInicial, lexema.length)
                            return false
                        }
                        devolverse(filaInicial, columnaInicial, lexema.length)
                        return false
                    }
                    devolverse(filaInicial, columnaInicial, lexema.length)
                    return false
                }
                devolverse(filaInicial, columnaInicial, lexema.length)
                return false
        }
        if(caracterActual.equals('l')) {
            lexema += caracterActual
            obtenerSiguienteCaracter()
            if(caracterActual.equals('i')) {
                lexema += caracterActual
                obtenerSiguienteCaracter()
                if(caracterActual.equals('m')) {
                    lexema += caracterActual
                    obtenerSiguienteCaracter()
                    saveToken(lexema, Categoria.PALABRA_RESERVADA, filaInicial, columnaInicial)
                    return true
                }
                devolverse(filaInicial, columnaInicial, lexema.length)
                return false
            }
            devolverse(filaInicial, columnaInicial, lexema.length)
            return false
        }
        if(caracterActual.equals('d')) {
            lexema += caracterActual
            obtenerSiguienteCaracter()
            if(caracterActual.equals('x')) {
                lexema += caracterActual
                obtenerSiguienteCaracter()
                saveToken(lexema, Categoria.PALABRA_RESERVADA, filaInicial, columnaInicial)
                return true
            }
            devolverse(filaInicial, columnaInicial, lexema.length)
            return false
        }
        if(caracterActual.equals('B')){
            lexema += caracterActual
            obtenerSiguienteCaracter()
            if(caracterActual.equals('o')) {
                lexema += caracterActual
                obtenerSiguienteCaracter()
                if(caracterActual.equals('o')) {
                    lexema += caracterActual
                    obtenerSiguienteCaracter()
                    if(caracterActual.equals('l')) {
                        lexema += caracterActual
                        obtenerSiguienteCaracter()
                        saveToken(lexema, Categoria.PALABRA_RESERVADA, filaInicial, columnaInicial)
                        return true
                    }
                    devolverse(filaInicial, columnaInicial, lexema.length)
                    return false
                }
                devolverse(filaInicial, columnaInicial, lexema.length)
                return false
            }
            devolverse(filaInicial, columnaInicial, lexema.length)
            return false
        }
        if(caracterActual.equals('I')){
            lexema += caracterActual
            obtenerSiguienteCaracter()
            if(caracterActual.equals('n')) {
                lexema += caracterActual
                obtenerSiguienteCaracter()
                if(caracterActual.equals('t')) {
                    lexema += caracterActual
                    obtenerSiguienteCaracter()
                    saveToken(lexema, Categoria.PALABRA_RESERVADA, filaInicial, columnaInicial)
                    return true
                }
                devolverse(filaInicial, columnaInicial, lexema.length)
                return false
            }
            devolverse(filaInicial, columnaInicial, lexema.length)
            return false
        }

        if(caracterActual.equals('S')) {
            lexema += caracterActual
            obtenerSiguienteCaracter()
            if(caracterActual.equals('t')) {
                lexema += caracterActual
                obtenerSiguienteCaracter()
                if(caracterActual.equals('r')) {
                    lexema += caracterActual
                    obtenerSiguienteCaracter()
                    if(caracterActual.equals('i')) {
                        lexema += caracterActual
                        obtenerSiguienteCaracter()
                        if(caracterActual.equals('n')) {
                            lexema += caracterActual
                            obtenerSiguienteCaracter()
                            if (caracterActual.equals('g')) {
                                lexema += caracterActual
                                obtenerSiguienteCaracter()
                                saveToken(lexema, Categoria.PALABRA_RESERVADA, filaInicial, columnaInicial)
                                return true
                            }
                            devolverse(filaInicial, columnaInicial, lexema.length)
                            return false
                        }
                        devolverse(filaInicial, columnaInicial, lexema.length)
                        return false
                    }
                    devolverse(filaInicial, columnaInicial, lexema.length)
                    return false
                }
                devolverse(filaInicial, columnaInicial, lexema.length)
                return false
            }
            devolverse(filaInicial, columnaInicial, lexema.length)
            return false
        }
        if(caracterActual.equals('D')) {
            lexema += caracterActual
            obtenerSiguienteCaracter()
            if(caracterActual.equals('e')) {
                lexema += caracterActual
                obtenerSiguienteCaracter()
                if(caracterActual.equals('c')) {
                    lexema += caracterActual
                    obtenerSiguienteCaracter()
                    if(caracterActual.equals('i')) {
                        lexema += caracterActual
                        obtenerSiguienteCaracter()
                        if(caracterActual.equals('m')) {
                            lexema += caracterActual
                            obtenerSiguienteCaracter()
                            if (caracterActual.equals('a')) {
                                lexema += caracterActual
                                obtenerSiguienteCaracter()
                                if(caracterActual.equals('l')) {
                                    lexema += caracterActual
                                    obtenerSiguienteCaracter()
                                    saveToken(lexema, Categoria.PALABRA_RESERVADA, filaInicial, columnaInicial)
                                    return true
                                }
                                devolverse(filaInicial, columnaInicial, lexema.length)
                                return false
                            }
                            devolverse(filaInicial, columnaInicial, lexema.length)
                            return false
                        }
                        devolverse(filaInicial, columnaInicial, lexema.length)
                        return false
                    }
                    devolverse(filaInicial, columnaInicial, lexema.length)
                    return false
                }
                devolverse(filaInicial, columnaInicial, lexema.length)
                return false
            }
            devolverse(filaInicial, columnaInicial, lexema.length)
            return false
        }
        if(caracterActual.equals('[')) {
            lexema += caracterActual
            obtenerSiguienteCaracter()
            if(caracterActual.equals('*')) {
                lexema += caracterActual
                obtenerSiguienteCaracter()
                if(caracterActual.equals(']')) {
                    lexema += caracterActual
                    obtenerSiguienteCaracter()
                    saveToken(lexema, Categoria.PALABRA_RESERVADA, filaInicial, columnaInicial)
                    return true
                }
                devolverse(filaInicial, columnaInicial, lexema.length)
                return false
            }
            devolverse(filaInicial, columnaInicial, lexema.length)
            return false
        }
        return false
    }

    fun esIdentificador(): Boolean {
        if(caracterActual.equals('@')){
            var pos = 0
            var lexema = ""
            var filaInicial = filaActual
            var columnaInicial = columnaActual
            lexema+=caracterActual
            obtenerSiguienteCaracter()
            pos++
            while (caracterActual.isDigit()||caracterActual.isLetter()&&!caracterActual.equals('@')) {
                lexema += caracterActual
                obtenerSiguienteCaracter()
                pos++
            }
            print(validarIdentificador(lexema))
            if(lexema[pos-1].isLetter()&&validarIdentificador(lexema)){
                saveToken(lexema,Categoria.IDENTIFICADOR,filaInicial,columnaInicial)
                return true
            }
            reportarError("el identificador debe terminar con una letra",filaInicial,columnaInicial)
            devolverse(filaInicial,columnaInicial,lexema.length)
            return false
        }
        return false
    }

    private fun esCaracterConcatenar(): Boolean{
        if(caracterActual.equals('&')){
            var filaInicial = filaActual
            var columnaInicial = columnaActual
            obtenerSiguienteCaracter()
            saveToken("&", Categoria.CARACTER_CONCATENAR,filaInicial,columnaInicial)
            return true
        }
        return false
    }

    fun validarIdentificador(palabra: String):Boolean{
        println()
        print(palabra)
        for (p in palabrasReservadas.iterator()) {
            if(palabra.contains(p)){
                return false
            }
        }
        return true
    }

    fun esEntero(): Boolean{
        if(caracterActual.equals('n')){
            var lexema = ""
            var filaInicial = filaActual
            var columnaInicial = columnaActual
            lexema+=caracterActual
            obtenerSiguienteCaracter()
            if(caracterActual.isDigit()){
                lexema+=caracterActual
                obtenerSiguienteCaracter()
                while (caracterActual.isDigit()){
                    lexema+=caracterActual
                    obtenerSiguienteCaracter()
                }
                saveToken(lexema,Categoria.ENTERO,filaInicial,columnaInicial)
                return true
            }
            devolverse(filaInicial,columnaInicial,lexema.length)
            return false
        }
        return false
    }
    fun esDecimal(): Boolean{
        var lexema = ""
        var filaInicial = filaActual
        var columnaInicial = columnaActual
        if(caracterActual.isDigit()){
            lexema+=caracterActual
            obtenerSiguienteCaracter()
            while (caracterActual.isDigit()){
                lexema+=caracterActual
                obtenerSiguienteCaracter()
            }
            if(caracterActual==',') {
                lexema+=caracterActual
                obtenerSiguienteCaracter()
                while (caracterActual.isDigit()) {
                    lexema += caracterActual
                    obtenerSiguienteCaracter()
                }
                if(caracterActual.equals('d')){
                    lexema+=caracterActual
                    obtenerSiguienteCaracter()
                    saveToken(lexema,Categoria.DECIMAL,filaInicial,columnaInicial)
                    return true
                }
                devolverse(filaInicial,columnaInicial,lexema.length)
                return false
            }
            devolverse(filaInicial,columnaInicial,lexema.length)
            return false
        }
        if(caracterActual.equals(',')){
            lexema+=caracterActual
            obtenerSiguienteCaracter()
            if(caracterActual.isDigit()){
                lexema+=caracterActual
                obtenerSiguienteCaracter()
                while (caracterActual.isDigit()){
                    lexema+=caracterActual
                    obtenerSiguienteCaracter()
                }
                if(caracterActual.equals('d')){
                    lexema+=caracterActual
                    obtenerSiguienteCaracter()
                    saveToken(lexema,Categoria.DECIMAL,filaInicial,columnaInicial)
                    return true
                }
                devolverse(filaInicial,columnaInicial,lexema.length)
                return false
            }
            devolverse(filaInicial,columnaInicial,lexema.length)
            return false
        }
        return false
    }
    fun esOperadorAritmetico(): Boolean{
        if(caracterActual.equals('+')||caracterActual.equals('-')||caracterActual.equals('*')||caracterActual.equals('/')||caracterActual.equals('%')){
            var lexema = ""
            var filaInicial = filaActual
            var columnaInicial = columnaActual
            lexema+=caracterActual
            obtenerSiguienteCaracter()
            if (caracterActual == '=') {
                devolverse(filaInicial, columnaInicial,lexema.length);
                return esOperadorDeAsignacion();
            }
            saveToken(lexema,Categoria.OPERADOR_ARITMETICO,filaInicial,columnaInicial)
            return true
        }
        return false
    }
    fun esOperadorDeAsignacion(): Boolean{
        var lexema = ""
        var filaInicial = filaActual
        var columnaInicial = columnaActual
        if(caracterActual.equals('+')||caracterActual.equals('-')||caracterActual.equals('*')||caracterActual.equals('/')||caracterActual.equals('%')){
            lexema+=caracterActual
            obtenerSiguienteCaracter()
            if(caracterActual.equals('=')){
                lexema+=caracterActual
                obtenerSiguienteCaracter()
                saveToken(lexema,Categoria.OPERADOR_ASIGNACION,filaInicial,columnaInicial)
                return true
            }
        }
        if(caracterActual.equals('=')){
            lexema+=caracterActual
            obtenerSiguienteCaracter()
            saveToken(lexema,Categoria.OPERADOR_ASIGNACION,filaInicial,columnaInicial)
            return true
        }
        return false
    }

    // +++, ---
    fun esOperadorIncrementoDecremento(): Boolean {

        var filaInicial = filaActual
        var columnaInicial: Int = columnaActual
        var lexema = ""

        if (caracterActual.equals('+')) {
            lexema += caracterActual
            obtenerSiguienteCaracter()
            if (caracterActual.equals('+')) {
                lexema += caracterActual
                obtenerSiguienteCaracter()
                if (caracterActual.equals('+')) {
                    lexema += caracterActual
                    saveToken(lexema, Categoria.OPERADOR_INCREMENTO, filaInicial, columnaInicial)
                    obtenerSiguienteCaracter()
                    return true
                }
                devolverse(filaInicial, columnaInicial, lexema.length);
                return false
            }
            devolverse(filaInicial, columnaInicial, lexema.length);
            return false
        }

        if (caracterActual.equals('-')) {
            lexema += caracterActual
            obtenerSiguienteCaracter()
            if (caracterActual.equals('-')) {
                lexema += caracterActual
                obtenerSiguienteCaracter()
                if (caracterActual.equals('-')) {
                    lexema += caracterActual
                    saveToken(lexema, Categoria.OPERADOR_DECREMENTO, filaInicial, columnaInicial)
                    obtenerSiguienteCaracter()
                    return true
                }
                devolverse(filaInicial, columnaInicial, lexema.length);
                return false
            }
            devolverse(filaInicial, columnaInicial, lexema.length);
            return false
        }

        //rechazo inmediato
        return false
    }


    fun esOperadorRelacional():Boolean{
        var lexema = ""
        var filaInicial = filaActual
        var columnaInicial = columnaActual
        if(caracterActual.equals('>')){
            lexema+=caracterActual
            obtenerSiguienteCaracter()
            if(caracterActual.equals('>')){
                lexema+=caracterActual
                obtenerSiguienteCaracter()
                if(caracterActual.equals('=')){
                    lexema+=caracterActual
                    obtenerSiguienteCaracter()
                }
                saveToken(lexema,Categoria.OPERADOR_RELACIONAL,filaInicial,columnaInicial)
                return true
            }
            if(caracterActual.equals('<')){
                lexema+=caracterActual
                obtenerSiguienteCaracter()
                saveToken(lexema,Categoria.OPERADOR_RELACIONAL,filaInicial,columnaInicial)
                return true
            }
            devolverse(filaInicial, columnaInicial,lexema.length);
            return false
        }
        if(caracterActual.equals('<')){
            lexema+=caracterActual
            obtenerSiguienteCaracter()
            if(caracterActual.equals('<')){
                lexema+=caracterActual
                obtenerSiguienteCaracter()
                if(caracterActual.equals('=')){
                    lexema+=caracterActual
                    obtenerSiguienteCaracter()
                }
                saveToken(lexema,Categoria.OPERADOR_RELACIONAL,filaInicial,columnaInicial)
                return true
            }
            if(caracterActual.equals('>')){
                lexema+=caracterActual
                obtenerSiguienteCaracter()
                saveToken(lexema,Categoria.OPERADOR_RELACIONAL,filaInicial,columnaInicial)
                return true
            }
            devolverse(filaInicial, columnaInicial,lexema.length);
            return false
        }
        return false
    }

    fun esOperadorLogico(): Boolean{
        var lexema = ""
        var filaInicial = filaActual
        var columnaInicial = columnaActual
        if(caracterActual.equals('a')){
            lexema+=caracterActual
            obtenerSiguienteCaracter()
            if(caracterActual.equals('n')){
                lexema+=caracterActual
                obtenerSiguienteCaracter()
                if(caracterActual.equals('d')) {
                    lexema += caracterActual
                    obtenerSiguienteCaracter()
                    saveToken(lexema, Categoria.OPERADOR_LOGICO, filaInicial, columnaInicial)
                    return true
                }
                devolverse(filaInicial, columnaInicial,lexema.length);
                return false
            }
            devolverse(filaInicial, columnaInicial,lexema.length);
            return false
        }
        if(caracterActual.equals('n')){
            lexema+=caracterActual
            obtenerSiguienteCaracter()
            if(caracterActual.equals('o')){
                lexema+=caracterActual
                obtenerSiguienteCaracter()
                if(caracterActual.equals('t')) {
                    lexema += caracterActual
                    obtenerSiguienteCaracter()
                    saveToken(lexema, Categoria.OPERADOR_LOGICO, filaInicial, columnaInicial)
                    return true
                }
                devolverse(filaInicial, columnaInicial,lexema.length);
                return false
            }
            devolverse(filaInicial, columnaInicial,lexema.length);
            return false
        }
        if(caracterActual.equals('o')){
            lexema+=caracterActual
            obtenerSiguienteCaracter()
            if(caracterActual.equals('r')){
                lexema+=caracterActual
                obtenerSiguienteCaracter()
                saveToken(lexema, Categoria.OPERADOR_LOGICO, filaInicial, columnaInicial)
                return true
            }
            devolverse(filaInicial, columnaInicial,lexema.length);
            return false
        }
        return false
    }

    fun esCaracter(): Boolean{
        if(caracterActual.equals('!')){
            var lexema = ""
            var filaInicial = filaActual
            var columnaInicial = columnaActual
            lexema+=caracterActual
            obtenerSiguienteCaracter()
            if(caracterActual.equals('!')){
                devolverse(filaInicial,columnaInicial,lexema.length)
                return esCadena()
            }
            while(!caracterActual.equals('!')){
                if (caracterActual !== finCodigo) {
                    lexema+= caracterActual;
                    obtenerSiguienteCaracter()
                } else {
                    print(lexema)
                    devolverse(filaInicial, columnaInicial, lexema.length);
                    return false;
                }
            }
            lexema+= caracterActual;
            obtenerSiguienteCaracter()
            saveToken(lexema,Categoria.CARACTER,filaInicial,columnaInicial)
            return true
        }
        return false
    }
    fun esCadena():Boolean{
        if (caracterActual.equals('!')) {
            var lexema = ""
            val fila = filaActual
            val col: Int = columnaActual
            lexema += caracterActual
            obtenerSiguienteCaracter()
            if (caracterActual.equals('!')) {
                lexema += caracterActual
                obtenerSiguienteCaracter()
                while (caracterActual != '!') {
                    if (caracterActual !== finCodigo) {
                        lexema += caracterActual
                        obtenerSiguienteCaracter()
                    } else {
                        devolverse(fila, col, lexema.length)
                        return false
                    }
                }
                if (lexema == "!!") {
                    devolverse(fila, col, lexema.length)
                    return false
                }
                lexema += caracterActual
                obtenerSiguienteCaracter()
                if(caracterActual == '!') {
                    lexema += caracterActual
                    obtenerSiguienteCaracter()
                    saveToken(lexema,Categoria.CADENA,filaActual,columnaActual)
                    return true
                }
                devolverse(fila, col, lexema.length)
                return false
            }
            devolverse(fila, col, lexema.length)
            return esCaracter()
        }
        return false
    }
    // |_
    // cos
    // _|
    fun esComentarioBloque(): Boolean{
        if(caracterActual.equals('|')){

            var lexema = ""
            var filaInicial = filaActual
            var columnaInicial = columnaActual

            lexema+=caracterActual
            obtenerSiguienteCaracter()

            if(caracterActual.equals('_')){
                lexema+=caracterActual
                obtenerSiguienteCaracter()

                while (!caracterActual.equals('_')){
                    if(!caracterActual.equals(finCodigo)){
                        lexema+=caracterActual
                        obtenerSiguienteCaracter()
                    }else{
                        devolverse(filaInicial, columnaInicial, lexema.length)
                        return false
                    }
                }

                lexema+=caracterActual
                obtenerSiguienteCaracter()

                if(caracterActual.equals('|')){
                    lexema+=caracterActual
                    saveToken(lexema, Categoria.COMENTARIO_BLOQUE, filaInicial, columnaInicial)
                    obtenerSiguienteCaracter()
                    return true
                }
                devolverse(filaInicial,columnaInicial,lexema.length)
                return false
            }
            devolverse(filaInicial,columnaInicial,lexema.length)
            return false
        }
        // rechazo inmediato
        return false
    }
    // |cos|
    fun esComentarioLinea(): Boolean {
        if(caracterActual.equals('|')){

            var lexema = ""
            var filaInicial = filaActual
            var columnaInicial = columnaActual

            lexema+=caracterActual
            obtenerSiguienteCaracter()

            while (!caracterActual.equals('|')){
                if(!caracterActual.equals(finCodigo)){
                    lexema+=caracterActual
                    obtenerSiguienteCaracter()
                }else{
                    devolverse(filaInicial, columnaInicial, lexema.length)
                    return false
                }
            }

            lexema+=caracterActual
            saveToken(lexema, Categoria.COMENTARIO_LINEA, filaInicial, columnaInicial)
            obtenerSiguienteCaracter()
            return true
        }
        // rechazo inmediato
        return false
    }
    fun esParentesisIzqDer(): Boolean {
        if (caracterActual.equals(')')) {
            saveToken(")", Categoria.PARENTESIS_DERECHO, filaActual, columnaActual)
            obtenerSiguienteCaracter()
            return true
        }
        if (caracterActual.equals('(')) {
            saveToken("(", Categoria.PARENTESIS_IZQUIERDO, filaActual, columnaActual)
            obtenerSiguienteCaracter()
            return true
        }
        return false
    }

    fun esLlavesIzqDer(): Boolean {
        if (caracterActual.equals('{')) {
            saveToken("{", Categoria.LLAVE_IZQUIERDA, filaActual, columnaActual)
            obtenerSiguienteCaracter()
            return true
        }
        if (caracterActual.equals('}')) {
            saveToken("}", Categoria.LLAVE_DERECHA, filaActual, columnaActual)
            obtenerSiguienteCaracter()
            return true
        }
        return false
    }


    fun esFinSentencia(): Boolean {
        if(caracterActual.equals(';')){
            var lexema = ""
            var filaInicial = filaActual
            var columnaInicial = columnaActual
            lexema+=caracterActual
            obtenerSiguienteCaracter()
            saveToken(lexema, Categoria.FIN_SENTENCIA, filaInicial, columnaInicial)
            return true
        }
        return false
    }

    fun obtenerSiguienteCaracter(){
        posicionActual++;
        if (posicionActual < codigoFuente.length) {
            if (caracterActual == '\n') {
                filaActual++;
                columnaActual = 0;
            } else {
                columnaActual++;
            }
            caracterActual = codigoFuente[posicionActual];
        } else {
            caracterActual = finCodigo;
        }
    }





}
