package co.edu.uniquindio.compiladores.lexico

class Token(var lexema: String, var categoria: Categoria, var fila: Int, var columna: Int) {

    override fun toString(): String {
        return "Token(lexema='$lexema', categoria=$categoria, fila=$fila, columna=$columna)"
    }

    fun getJavaCode():String{

        //TODO cuadrar segun nuestros tipos de datos, revisar las diferencias en todos los operadores
        if(categoria == Categoria.PALABRA_RESERVADA){

            when(lexema){
                "Int"->{
                    return "int";
                }
                "Decimal"->{
                    return  "double"
                }
                "Bool"->{
                    return "boolean"
                }
                "Char" -> {
                    return "char"
                }
                "R" -> {
                    return "true"
                }
                "F" -> {
                    return "false"
                }
            }

        }else if(categoria == Categoria.OPERADOR_RELACIONAL) {

            when (lexema) {

                ">>" -> {
                    return ">"
                }
                "<<" -> {
                    return "<"
                }
                ">>="->{
                    return ">="
                }
                "<<=" ->{
                    return "<="
                }
                "><" ->{
                    return "!="
                }
                "<>" ->{
                    return "=="
                }


            }
        }else if(categoria == Categoria.OPERADOR_LOGICO) {

            when (lexema) {
                "and" ->{
                    return "&&"
                }
                "or" ->{
                    return "||"
                }
                "not" ->{
                    return "!"
                }
            }
        }
        else if(categoria == Categoria.CADENA){
            // adaptado
            var salida=lexema.substring(2,lexema.length-2)
            salida = "\""+lexema+"\""
            return salida;

        }else if(categoria == Categoria.IDENTIFICADOR){
            //adaptado
            return  lexema.replace('@','$')

        }else if(categoria == Categoria.ENTERO){

            return lexema.substring(1,lexema.length)
        }
        else if(lexema == "loop"){

            return "while"
        }
        return lexema
    }

}