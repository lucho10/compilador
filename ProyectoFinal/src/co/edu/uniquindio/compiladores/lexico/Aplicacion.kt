package co.edu.uniquindio.compiladores.lexico

fun main() {
    val lexico = AnalizadorLexico("fx sumar(int @n1, int @n2): int")
    lexico.analizar()
    print(lexico.tokens)
}