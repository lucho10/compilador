package co.edu.uniquindio.compiladores.sintaxis

import co.edu.uniquindio.compiladores.lexico.Token
import javafx.scene.control.TreeItem

class ValorNumerico(var signo:Token?,var valor:Token): Expresion() {

    override fun toString(): String {
        return "ValorNumerico(signo=$signo, valor=$valor)"
    }

    override fun getArbolVisual(): TreeItem<String> {
        var raiz = TreeItem("Valor Numerico")
        var variableRaiz = TreeItem("Signo")
        var v = if (signo != null) signo!!.lexema else "+"
        variableRaiz.children.add(TreeItem(v))
        raiz.children.add(variableRaiz)

        var operadorRaiz = TreeItem("Valor")
        operadorRaiz.children.add(TreeItem(valor.lexema))
        raiz.children.add(operadorRaiz)
        return raiz
    }

    override fun getJavaCode(): String {

        return if(signo!==null){
            signo!!.getJavaCode() + " " +valor.getJavaCode()
        }else{
            valor.getJavaCode()
        }
    }
}