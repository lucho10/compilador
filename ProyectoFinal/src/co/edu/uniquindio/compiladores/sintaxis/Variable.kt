package co.edu.uniquindio.compiladores.sintaxis

import co.edu.uniquindio.compiladores.lexico.Error
import co.edu.uniquindio.compiladores.lexico.Token
import co.edu.uniquindio.compiladores.semantica.TablaSimbolos
import javafx.scene.control.TreeItem

class Variable(var identificador: Token): Expresion() {

    override fun getArbolVisual(): TreeItem<String> {
        var raiz = TreeItem("Variable")
        raiz.children.add(TreeItem(identificador.lexema))
        return raiz
    }

    override fun getTipo(tablaSimbolos: TablaSimbolos, listaErrores: ArrayList<Error>, ambito: String): String {
        var s = tablaSimbolos.buscarSimboloValor(identificador.lexema,ambito,identificador.fila,identificador.columna)
        if(s==null){
            listaErrores.add(Error("La variable ${identificador.lexema} no existe en el ambito ${ambito}", identificador.fila,identificador.columna))
        } else {
            return s.tipo
        }
        return ""
    }

}