package co.edu.uniquindio.compiladores.sintaxis

import co.edu.uniquindio.compiladores.lexico.Error
import co.edu.uniquindio.compiladores.lexico.Token
import co.edu.uniquindio.compiladores.semantica.TablaSimbolos
import javafx.scene.control.TreeItem

class Retorno(var token: Token, var expresion: Expresion): Sentencia() {
    //identificador | entero | decimal | cadena | caracter | <InvocacionFuncion> | <ExpresionAtitmetica> | <ExpresionLogica>

   override fun getArbolVisual(): TreeItem<String> {
        var raiz = TreeItem("Retorno")
        raiz.children.add(expresion.getArbolVisual())
        return raiz
   }

    override fun toString(): String {
        return "Retorno(expresion=$expresion)"
    }

    override fun analizarSemantica(tablaSimbolos: TablaSimbolos, listaErrores: ArrayList<Error>, ambito: String) {
        expresion.analizarSemantica(tablaSimbolos,listaErrores,ambito)
        val tipoExpresion = expresion.getTipo(tablaSimbolos,listaErrores,ambito)
        println("que retornaaaaaaa "+tipoExpresion)
        println("en que ambito "+ambito)
        val parts = ambito.split(',')
        val tiposParametros = build(parts[1].substring(1,parts[1].length-1))
        val funcion = tablaSimbolos.buscarSimboloFuncion(parts[0],tiposParametros)
        println("funcion encontrada "+funcion)
        if(funcion!=null){
            val tipoFuncion = funcion.tipo
            if(tipoFuncion != tipoExpresion){
                listaErrores.add(Error("El tipo de retorno de la funcion (${funcion.nombre}) (${tipoFuncion}) no coincide con el retorno de la expresion ya que es (${tipoExpresion})",token.fila,token.columna))
            }
        }
    }

    private fun build(cadena: String):ArrayList<String> {
        var lista = ArrayList<String>()
        val listaParametros = cadena.split(',')
        for (p in listaParametros){
            lista.add(p)
        }
        return lista
    }

    override fun getJavaCode(): String {
        return "return "+ expresion.getJavaCode()+";"
    }

}