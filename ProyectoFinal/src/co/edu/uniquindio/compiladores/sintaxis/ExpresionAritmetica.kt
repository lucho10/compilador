package co.edu.uniquindio.compiladores.sintaxis

import co.edu.uniquindio.compiladores.lexico.Categoria
import co.edu.uniquindio.compiladores.lexico.Error
import co.edu.uniquindio.compiladores.lexico.Token
import co.edu.uniquindio.compiladores.semantica.TablaSimbolos
import javafx.scene.control.TreeItem

class ExpresionAritmetica() : Expresion() {

    var expresion1: ExpresionAritmetica? = null
    var expresion2: ExpresionAritmetica? = null
    var operador: Token? = null
    var numerico:ValorNumerico? = null



    // Constructores secundarios
    constructor(expresion1: ExpresionAritmetica?, operador: Token?, expresion2: ExpresionAritmetica?) : this() {
        this.expresion1 = expresion1
        this.operador = operador
        this.expresion2 = expresion2
    }
    constructor(numerico: ValorNumerico?, operador: Token?, expresion: ExpresionAritmetica?) : this() {
        this.numerico = numerico
        this.operador = operador
        this.expresion2 = expresion
    }

    constructor(expresion: ExpresionAritmetica?) : this() {
        this.expresion1 = expresion
    }

    constructor(numerico: ValorNumerico?) : this() {
        this.numerico = numerico
    }

    override fun getArbolVisual(): TreeItem<String> {
        var raiz = TreeItem("Expresion Aritmetica")

        if(expresion1!=null && operador!=null && expresion2!=null){
            raiz.children.add(expresion1!!.getArbolVisual())
            raiz.children.add(TreeItem(operador!!.lexema))
            raiz.children.add(expresion2!!.getArbolVisual())
        } else if(expresion1!=null && operador == null && expresion2==null && numerico==null){
            raiz.children.add(expresion1!!.getArbolVisual())
        } else if(numerico!=null && operador!=null && expresion2!=null){
            raiz.children.add(numerico!!.getArbolVisual())
            raiz.children.add(TreeItem(operador!!.lexema))
            raiz.children.add(expresion2!!.getArbolVisual())
        } else {
            raiz.children.add(numerico!!.getArbolVisual())
        }

        return raiz
    }


    override fun getTipo(tablaSimbolos: TablaSimbolos, listaErrores: ArrayList<Error>, ambito:String): String {
        if(expresion1!=null && expresion2!=null){
            var tipo1 = expresion1!!.getTipo(tablaSimbolos,listaErrores,ambito)
            var tipo2 = expresion2!!.getTipo(tablaSimbolos,listaErrores,ambito)
            return getTipo(tipo1,tipo2)
        } else if(expresion1!=null){
            return expresion1!!.getTipo(tablaSimbolos,listaErrores,ambito)
        } else if(numerico!=null && expresion2 !=null){
             val tipo1 = getTipoNumerico(numerico!!,tablaSimbolos,listaErrores,ambito)
             var tipo2 = expresion2!!.getTipo(tablaSimbolos,listaErrores,ambito)
             println("tipo2 "+tipo2)
             return getTipo(tipo1,tipo2)
        } else if(numerico!=null){
             return getTipoNumerico(numerico!!,tablaSimbolos,listaErrores,ambito)
        }
        return ""
    }

    private fun getTipoNumerico(numerico: ValorNumerico, tablaSimbolos: TablaSimbolos, listaErrores: ArrayList<Error>, ambito: String): String{
        if(numerico.valor.categoria == Categoria.ENTERO){
            return "Int"
        } else if(numerico.valor.categoria == Categoria.DECIMAL){
            return "Decimal"
        } else {
            var simbolo = tablaSimbolos.buscarSimboloValor(numerico.valor.lexema,ambito,numerico.valor.fila,numerico.valor.fila)
            if(simbolo!=null){
                return simbolo.tipo
            }
        }
        return ""
    }

    private fun getTipo(tipo1:String, tipo2: String):String{
        if(tipo1 == "" || tipo2 == ""){
            return ""
        }
        return if(tipo1 == "Decimal" || tipo2 == "Decimal"){
            "Decimal"
        } else {
            "Int"
        }
    }

    override fun analizarSemantica(tablaSimbolos: TablaSimbolos, listaErrores: ArrayList<Error>, ambito: String) {
        if(numerico!=null){
            println("valor numerico "+numerico!!.valor.lexema)
            if(numerico!!.valor.categoria == Categoria.IDENTIFICADOR){
                var simbolo = tablaSimbolos.buscarSimboloValor(numerico!!.valor.lexema,ambito,numerico!!.valor.fila,numerico!!.valor.columna)
                if(simbolo==null){
                    println("lista de errores en expresion arimetica")

                    listaErrores.add(Error("El campo (${numerico!!.valor.lexema}) no existe en el ambito ($ambito)",
                    numerico!!.valor.fila,numerico!!.valor.columna))
                    println(listaErrores.size)
                }
                else if(simbolo!!.tipo != "Int" && simbolo!!.tipo != "Decimal"){
                    listaErrores.add(Error("El tipo de dato de (${numerico!!.valor.lexema}) no es valido para una expresion aritmetica en el ambito ($ambito)",
                        numerico!!.valor.fila,numerico!!.valor.columna))
                }
            }
        }
        if(expresion1!=null){
            expresion1!!.analizarSemantica(tablaSimbolos,listaErrores,ambito)
        }
        if(expresion2!=null){
            expresion2!!.analizarSemantica(tablaSimbolos,listaErrores,ambito)
        }

    }
    override fun getJavaCode(): String {

        return if(expresion1!=null && operador!=null && expresion2!=null){
            expresion1!!.getJavaCode() + operador!!.getJavaCode()+expresion2!!.getJavaCode()
        } else if(expresion1!=null && operador == null && expresion2==null && numerico==null){
            expresion1!!.getJavaCode()
        } else if(numerico!=null && operador!=null && expresion2!=null){
            numerico!!.getJavaCode()+operador!!.getJavaCode()+expresion2!!.getJavaCode()
        }else{
            numerico!!.getJavaCode()
        }
    }




}