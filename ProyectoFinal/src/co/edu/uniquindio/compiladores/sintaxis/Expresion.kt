package co.edu.uniquindio.compiladores.sintaxis

import co.edu.uniquindio.compiladores.lexico.Error
import co.edu.uniquindio.compiladores.semantica.TablaSimbolos
import javafx.scene.control.TreeItem

open class Expresion {

    open fun getArbolVisual(): TreeItem<String>{
        return TreeItem("Expresion")
    }

    open fun analizarSemantica(tablaSimbolos: TablaSimbolos, listaErrores: ArrayList<Error>, ambito: String){}

    open fun getTipo(tablaSimbolos: TablaSimbolos, listaErrores: ArrayList<Error>,ambito:String): String{
          return ""
    }

    open fun getJavaCode(): String {
        return ""
    }

}