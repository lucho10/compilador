package co.edu.uniquindio.compiladores.sintaxis

import co.edu.uniquindio.compiladores.lexico.Token
import javafx.scene.control.TreeItem

class Impresion(var dato: Token): Sentencia() {
    override fun toString(): String {
        return "Lectura(Dato=$dato)"
    }

    override fun getArbolVisual(): TreeItem<String> {
        var raiz = TreeItem("Impresion")
        var variableRaiz = TreeItem(dato.lexema)
        raiz.children.add(variableRaiz)
        return raiz
    }

    override fun getJavaCode(): String {
        return "JOPtionPane.showMessageDialog(null"+dato.getJavaCode()+")"
    }

}