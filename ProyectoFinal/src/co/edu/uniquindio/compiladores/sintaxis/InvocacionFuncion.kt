package co.edu.uniquindio.compiladores.sintaxis

import co.edu.uniquindio.compiladores.lexico.Error
import co.edu.uniquindio.compiladores.lexico.Token
import co.edu.uniquindio.compiladores.semantica.TablaSimbolos
import javafx.scene.control.TreeItem


class InvocacionFuncion(var nombreFuncion: Token, var listaArgumentos:ArrayList<Expresion>): Expresion() {
    override fun toString(): String {
        return "InvocacionFuncion(nombreFuncion=$nombreFuncion, listaArgumentos=$listaArgumentos)"
    }
    override fun getArbolVisual(): TreeItem<String> {
        var raiz = TreeItem("Invocacion funcion")
        raiz.children.add(TreeItem("nombre: ${nombreFuncion.lexema}"))
        var raizParametros = TreeItem("Parametros")
        for (p in listaArgumentos){
            raizParametros.children.add(p.getArbolVisual())
        }
        raiz.children.add(raizParametros)
        return raiz
    }

    private fun getTiposArgumentos(tablaSimbolos: TablaSimbolos, listaErrores: ArrayList<Error>, ambito: String): ArrayList<String>{
        var list = ArrayList<String>()
        for (a in listaArgumentos){
            list.add(a.getTipo(tablaSimbolos,listaErrores,ambito))
        }
        return list
    }

    override fun analizarSemantica(tablaSimbolos: TablaSimbolos, listaErrores: ArrayList<Error>, ambito: String) {
        var lista = getTiposArgumentos(tablaSimbolos,listaErrores,ambito)
        var s = tablaSimbolos.buscarSimboloFuncion(nombreFuncion.lexema, lista)
        if(s==null){
            listaErrores.add(Error("La funcion ${nombreFuncion.lexema} $lista no existe", nombreFuncion.fila,nombreFuncion.columna))
        }
    }

    override fun getTipo(tablaSimbolos: TablaSimbolos, listaErrores: ArrayList<Error>, ambito: String): String {
        var lista = getTiposArgumentos(tablaSimbolos,listaErrores,ambito)
        var s = tablaSimbolos.buscarSimboloFuncion(nombreFuncion.lexema, lista)
        if(s!=null){
            return s.tipo
        }
        return ""
    }

    /**
     * <InvocacionFuncion> ::= nombreFuncion "("[<ListaArgumentos>]")"
     */
    override fun getJavaCode(): String {

        var codigo = nombreFuncion.getJavaCode()
        codigo+="("
        if(listaArgumentos.isNotEmpty()) {

            for (arg in listaArgumentos) {

                codigo += arg.getJavaCode()+","
            }
            codigo = codigo.substring(0,codigo.length-1)
        }
        codigo+=")"

        return codigo
    }


}