package co.edu.uniquindio.compiladores.sintaxis

import co.edu.uniquindio.compiladores.lexico.Error
import co.edu.uniquindio.compiladores.lexico.Token
import co.edu.uniquindio.compiladores.semantica.TablaSimbolos
import javafx.scene.control.TreeItem

class Arreglo (var identifador: Token, var tipoDato: Token, var listaExpresiones: ArrayList<Expresion>) : Sentencia() {

    override fun getArbolVisual(): TreeItem<String> {
        val raiz = TreeItem("Arreglo")
        val arreglo = TreeItem("${identifador.lexema} : ${tipoDato.lexema}" )
        if(listaExpresiones.isEmpty()){
            val raizD = TreeItem("Declaracion")
            raizD.children.add(arreglo)
            raiz.children.add(raizD)
        } else {
            val raizA = TreeItem("Asignacion")
            raizA.children.add(TreeItem("${identifador.lexema} : ${tipoDato.lexema}" ))
            val raizExp = TreeItem("Argumentos")
            for (ex in listaExpresiones){
                raizExp.children.add(ex.getArbolVisual())
            }
            raizA.children.add(raizExp)
            raiz.children.add(raizA)
        }

        return raiz
    }

    override fun toString(): String {
        return "Arreglo(identifador=$identifador, tipoDato=$tipoDato, listaExpresiones=$listaExpresiones)"
    }

    override fun llenarTablaSimbolos(tablaSimbolos: TablaSimbolos, listaErrores: ArrayList<Error>, ambito: String) {
        tablaSimbolos.guardarSimboloValor(identifador.lexema,tipoDato.lexema,true,ambito,identifador.fila,identifador.columna)
    }

    override fun analizarSemantica(tablaSimbolos: TablaSimbolos, listaErrores: ArrayList<Error>, ambito: String) {

        for (e in listaExpresiones){
              e!!.analizarSemantica(tablaSimbolos,listaErrores,ambito)
              val tipo = e.getTipo(tablaSimbolos,listaErrores,ambito)
              println("tipo "+ tipo)
              if(tipo != tipoDato.lexema){
                   listaErrores.add(Error("El tipo de dato de la expresion ($tipo) no coincide con el tipo de dato del arreglo (${tipoDato.lexema})",identifador.fila,identifador.columna))
              }
        }

    }
    override fun getJavaCode(): String {

        var codigo = tipoDato.getJavaCode() + "[]" + identifador.getJavaCode();

        if(listaExpresiones!=null){

            codigo+="={"
            for( e in listaExpresiones ){
                codigo += e.getJavaCode()+","
            }
            codigo = codigo.substring(0,codigo.length-1)
            codigo+="}"

        }
        codigo +=";"

        return codigo
    }
}