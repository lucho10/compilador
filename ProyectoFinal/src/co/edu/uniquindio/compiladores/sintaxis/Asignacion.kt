package co.edu.uniquindio.compiladores.sintaxis

import co.edu.uniquindio.compiladores.lexico.Error
import co.edu.uniquindio.compiladores.lexico.Token
import co.edu.uniquindio.compiladores.semantica.TablaSimbolos
import javafx.scene.control.TreeItem

class Asignacion(var identificador: Token, var operador: Token): Sentencia() {

    var expresion: Expresion? = null

    constructor(identifador: Token, operador: Token, expresion: Expresion):this(identifador,operador){
          this.expresion = expresion
    }

    override fun getArbolVisual(): TreeItem<String> {
        var raiz = TreeItem("Asignacion")
        var variableRaiz = TreeItem("Identificador")
        variableRaiz.children.add(TreeItem(identificador.lexema))
        raiz.children.add(variableRaiz)

        var operadorRaiz = TreeItem("Operador")
        operadorRaiz.children.add(TreeItem("${operador.lexema}"))
        raiz.children.add(operadorRaiz)

        if(expresion!=null){
            var expresionRaiz = TreeItem("Expresion")
            expresionRaiz.children.add(expresion!!.getArbolVisual())
            raiz.children.add(expresionRaiz)
        }

        return raiz
    }

    override fun toString(): String {
        return "Asignacion(nombreVariable=$identificador, operador=$operador, expresion=$expresion)"
    }

    override fun analizarSemantica(tablaSimbolos: TablaSimbolos, listaErrores: ArrayList<Error>, ambito: String) {
        val s = tablaSimbolos.buscarSimboloValor(identificador.lexema,ambito,identificador.fila,identificador.columna)
        if(s==null){
            listaErrores.add(Error("El campo ${identificador.lexema} no existe en el ambito ${ambito}", identificador.fila,identificador.columna))
        } else {
            var tipo = s.tipo
            if(expresion!=null){
                 expresion!!.analizarSemantica(tablaSimbolos,listaErrores,ambito)
                var tipoExp = expresion!!.getTipo(tablaSimbolos,listaErrores,ambito)
                if(tipoExp!=tipo){
                    listaErrores.add(Error("El tipo de dato de la expresion ($tipoExp) no coincide con el tipo de dato de la variable (${identificador.lexema}) ya que es $tipo",identificador.fila,identificador.columna))
                }
            }
        }
    }

    override fun getJavaCode(): String {
        var codigo = ""
        codigo += "${identificador.getJavaCode()} ${operador.getJavaCode()} ${expresion!!.getJavaCode()};"
        return codigo
    }
}