package co.edu.uniquindio.compiladores.sintaxis

import co.edu.uniquindio.compiladores.lexico.Error
import co.edu.uniquindio.compiladores.lexico.Token
import co.edu.uniquindio.compiladores.semantica.TablaSimbolos
import javafx.scene.control.TreeItem

class Funcion(var nombreFuncion:Token,var listaParametros: ArrayList<Parametro> , var tipoRetorno: Token,var listaSensencias: ArrayList<Sentencia>) {

    override fun toString(): String {
        return "Funcion(nombreFuncion=$nombreFuncion, listaParametros=$listaParametros, tipoRetorno=$tipoRetorno, listaSensencias=$listaSensencias)"
    }
    fun getArbolVisual(): TreeItem<String> {
        var raiz = TreeItem("Funcion")
        raiz.children.add(TreeItem("nombre: ${nombreFuncion.lexema}"))
        raiz.children.add(TreeItem("Tipo Retorno: ${tipoRetorno.lexema}"))

        var raizParametros = TreeItem("Parametros")
        for (p in listaParametros){
                raizParametros.children.add(p.getArbolVisual())
        }
        raiz.children.add(raizParametros)

        var raizSentencias = TreeItem("Sentencias")
        for (s in listaSensencias){
            raizSentencias.children.add(s.getArbolVisual())
        }
        raiz.children.add(raizSentencias)

        return raiz
    }

    private fun obtenerTiposParametros():ArrayList<String>{
          var lista = ArrayList<String>()

          for (p in listaParametros){
              lista.add(p.tipoDato.lexema)
          }
        return lista
    }

    fun llenarTablaSimbolos(tablaSimbolos: TablaSimbolos, listaErrores:ArrayList<Error>,ambito: String){
         tablaSimbolos.guardarSimboloFuncion(nombreFuncion.lexema,tipoRetorno.lexema,obtenerTiposParametros(),ambito,nombreFuncion.fila,nombreFuncion.columna)

         for (p in listaParametros){
             tablaSimbolos.guardarSimboloValor(p.identificador.lexema,p.tipoDato.lexema,true,this.getAmbito(),p.identificador.fila,p.identificador.columna)
         }

         for (s in listaSensencias){
             s.llenarTablaSimbolos(tablaSimbolos,listaErrores,this.getAmbito())
         }

    }

    fun analizarSemantica(tablaSimbolos: TablaSimbolos,listaErrores:ArrayList<Error>){
        val funcion = tablaSimbolos.buscarSimboloFuncion(this.nombreFuncion.lexema,obtenerTiposParametros())

        if(funcion!!.tipo == "Int" || funcion!!.tipo == "Decimal" || funcion!!.tipo == "String"){

        }


        for (s in listaSensencias){
            s.analizarSemantica(tablaSimbolos,listaErrores,this.getAmbito())
        }

    }

    private fun getAmbito():String{
        return "${nombreFuncion.lexema},${obtenerTiposParametros()}"
    }

    fun getJavaCode(): String {

        // TODO cuando sea no se avoid ir a la semantica capturar el tipo de retorno y asignarala a la funcion ?
        var codigo = ""
        //TODO acomoda rel metodo principal de nuestro lenguaje y validar en la semantica que alla un metodo
        //principal
        if (nombreFuncion.getJavaCode() == "\$main") {
            codigo = "public static void main( String[] arg){"
        } else {

            codigo = "static "+tipoRetorno.getJavaCode() + " " + nombreFuncion.getJavaCode() + "("

            if(listaParametros.isNotEmpty()) {
                for (p in listaParametros) {
                    codigo += p.getJavaCode() + ","
                }
                codigo = codigo.substring(0, codigo.length - 1)
            }
            //para quitar la ultima coma
            codigo += "){"

        }
        for (s in listaSensencias) {
            codigo += s.getJavaCode()
        }
        codigo += "}"

        return codigo
    }

}