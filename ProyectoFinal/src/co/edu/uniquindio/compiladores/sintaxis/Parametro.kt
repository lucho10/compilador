package co.edu.uniquindio.compiladores.sintaxis

import co.edu.uniquindio.compiladores.lexico.Error
import co.edu.uniquindio.compiladores.lexico.Token
import co.edu.uniquindio.compiladores.semantica.TablaSimbolos
import javafx.scene.control.TreeItem

class Parametro( var identificador: Token,var tipoDato: Token) {

    override fun toString(): String {
        return "Parametro(identificador=$identificador, tipoDato=$tipoDato)"
    }

    fun getArbolVisual(): TreeItem<String>{
        return TreeItem("${identificador.lexema} : ${tipoDato.lexema}")
    }

    fun getJavaCode():String{
        return tipoDato.getJavaCode() + " " + identificador.getJavaCode()
    }



}