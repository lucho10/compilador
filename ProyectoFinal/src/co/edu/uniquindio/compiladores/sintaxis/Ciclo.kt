package co.edu.uniquindio.compiladores.sintaxis

import co.edu.uniquindio.compiladores.lexico.Error
import co.edu.uniquindio.compiladores.semantica.TablaSimbolos
import javafx.scene.control.TreeItem

class Ciclo(var expresionLogica: ExpresionLogica, var listaSentencia: ArrayList<Sentencia>?) : Sentencia() {

    override fun toString(): String {
        return "Ciclo(expresionLogica=$expresionLogica, listaSentencias=$listaSentencia)"
    }

    override fun getArbolVisual(): TreeItem<String> {
        var raiz  = TreeItem("Ciclo")

        var condicion = TreeItem("Condicion")
        condicion.children.add(expresionLogica.getArbolVisual())

        raiz.children.add(condicion)

        if(listaSentencia!=null){
            var raizSentencias = TreeItem("Sentencias")
            for (s in listaSentencia!!){
                raizSentencias.children.add(s.getArbolVisual())
            }
            raiz.children.add(raizSentencias)
        }
        return raiz
    }

    override fun llenarTablaSimbolos(tablaSimbolos: TablaSimbolos, listaErrores: ArrayList<Error>, ambito: String) {
        for (s in listaSentencia!!){
            s.llenarTablaSimbolos(tablaSimbolos,listaErrores,ambito)
        }
    }

    override fun analizarSemantica(tablaSimbolos: TablaSimbolos, listaErrores: ArrayList<Error>, ambito: String) {
        expresionLogica.analizarSemantica(tablaSimbolos,listaErrores,ambito)
        for (s in listaSentencia!!){
            s.analizarSemantica(tablaSimbolos,listaErrores,ambito)
        }
    }

    override fun getJavaCode(): String{

        var codigo = "while(" + expresionLogica.getJavaCode() + "){"

        for (s in this.listaSentencia!!){
            codigo +=s.getJavaCode()
        }
        codigo+="}"

        return codigo
    }

}