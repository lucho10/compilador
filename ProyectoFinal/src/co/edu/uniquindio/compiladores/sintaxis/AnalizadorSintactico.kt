package co.edu.uniquindio.compiladores.sintaxis

import co.edu.uniquindio.compiladores.lexico.Categoria
import co.edu.uniquindio.compiladores.lexico.Error
import co.edu.uniquindio.compiladores.lexico.Token
import kotlin.math.exp
import kotlin.system.exitProcess

class AnalizadorSintactico(var listaTokens: ArrayList<Token>) {

    var posicionActual = 0
    var tokenActual = listaTokens[posicionActual]
    var listaErrores = ArrayList<Error>()

    private fun obtenerSiguienteToken(){

        posicionActual++;

        if(posicionActual < listaTokens.size){
            tokenActual = listaTokens[posicionActual]
        }

    }

    private fun reportarError(mensaje : String){
           listaErrores.add(Error(mensaje, tokenActual.fila, tokenActual.columna ))
    }

    fun devolverse() {
        posicionActual-= 1
        tokenActual = listaTokens[posicionActual]
    }

    fun back(pos: Int){
        posicionActual = pos
        tokenActual = listaTokens[posicionActual]
    }
    

    /**
     * <UnidadDeCompilacion> ::= <ListaFunciones>
     */

    fun esUnidadDeCompilacion(): UnidadDeCompilacion? {
        val listaFunciones: ArrayList<Funcion> = esListaFunciones()
        return if (listaFunciones.size > 0) {
            UnidadDeCompilacion(listaFunciones)
        } else null
    }

    /**
     * <ListaFunciones> ::= <Funcion>[<ListaFunciones>]
     */

     private fun esListaFunciones(): ArrayList<Funcion> {

        var listaFunciones = ArrayList<Funcion>()
        var funcion = esFuncion()
        while (funcion!=null) {
             listaFunciones.add(funcion)
             println(listaFunciones.toString())
             println(tokenActual)
             funcion = esFuncion()
        }
        return listaFunciones
    }

    /**
     * <Funcion> ::= fx identificador "("[<ListaParametros>]")" [":"<TipoRetorno>]
       <BloqueSentencias>
     */

    private fun esFuncion():Funcion?{
        if(tokenActual.categoria == Categoria.PALABRA_RESERVADA && tokenActual.lexema == "fx"){
            obtenerSiguienteToken()
                if(tokenActual.categoria== Categoria.IDENTIFICADOR){
                      var nombreFuncion = tokenActual
                      obtenerSiguienteToken()
                      if(tokenActual.categoria == Categoria.PARENTESIS_IZQUIERDO) {
                          obtenerSiguienteToken()
                      }  else{
                           reportarError("Falta el parentesis izquierdo en la funcion")
                      }
                            var listaParametros = esListaParametros()
                          if(tokenActual.categoria == Categoria.PARENTESIS_DERECHO) {
                              obtenerSiguienteToken()

                          } else {
                              reportarError("Falta el parentesis derecho en la funcion")
                          }
                              if(tokenActual.categoria == Categoria.DOS_PUNTOS){
                                  obtenerSiguienteToken()
                                  var tipoRetorno = esTipoRetorno()
                                  if(tipoRetorno!=null){
                                      obtenerSiguienteToken()
                                      var listaSentencias = esBloqueSentencias()
                                      if(listaSentencias!=null){
                                           obtenerSiguienteToken()
                                           return Funcion(nombreFuncion,listaParametros,tipoRetorno,listaSentencias)
                                      } else {
                                          println("toki "+tokenActual.lexema)
                                          modoPanico()
                                          return esFuncion()
                                          reportarError("el bloque de sentencias esta vacio")
                                      }
                                  }else{
                                      reportarError("Falta el tipo de retorno de la funcion")
                                  }
                              } else {
                                  reportarError("Faltan los dos puntos")
                              }


                 } else {
                    reportarError("identicador no valido para la funcion")
                }
        }
        return null
    }

    fun modoPanico(){
        /**
         *  while (tokenActual.lexema !== "fx" && tokenActual.categoria !== Categoria.PALABRA_RESERVADA){
        println("lexemas "+ tokenActual.lexema)
        obtenerSiguienteToken()
        }
         */
        while (tokenActual.lexema !== "}"){
            println("lexemas "+ tokenActual.lexema)
            obtenerSiguienteToken()
        }
    }

    /**
     *  <ListaParametros> ::= <Parametro>[","<ListaParametros>]
     */
     private fun esListaParametros(): ArrayList<Parametro>{
        var listaParametros = ArrayList<Parametro>()

        if (tokenActual.categoria !== Categoria.PARENTESIS_DERECHO) {
            var parametro = esParametro()
            while (parametro != null) {
                listaParametros.add(parametro)
                if (tokenActual.categoria == Categoria.COMA) {
                    obtenerSiguienteToken()
                    parametro = esParametro()
                } else {
                    if(tokenActual.categoria !== Categoria.PARENTESIS_DERECHO) {
                        reportarError("Falta la coma en los parametros")
                    }
                    break
                }
            }
        }
        return listaParametros
    }

    /**
     * <Parametro> ::= identificador":"<TipoDato>
     */
    private fun esParametro(): Parametro?{
        if(tokenActual.categoria == Categoria.IDENTIFICADOR){
               var identificador = tokenActual
               obtenerSiguienteToken()
               if(tokenActual.categoria == Categoria.DOS_PUNTOS){
                   obtenerSiguienteToken()
                   var tipoDato = esTipoDato()
                   if(tipoDato!=null){
                       var tipoDato = tokenActual
                       obtenerSiguienteToken()
                       return Parametro(identificador,tipoDato)

                   } else {
                       reportarError("Falta el tipo de dato en el parametro")
                   }
               } else {
                   reportarError("Falta el operador :")
               }
        } else {
            reportarError("Falta el identificador en el parametro")
        }
        return null
    }

    /**
     * <TipoDato> ::= Int | Decimal | String | Char | Bool
     */
    private fun esTipoDato(): Token?{

        if(tokenActual.categoria == Categoria.PALABRA_RESERVADA && tokenActual.lexema == "Int" || tokenActual.lexema == "Decimal"||tokenActual.lexema == "String" ||tokenActual.lexema == "Char" ||tokenActual.lexema == "Bool"){
            return  tokenActual
        } else {
            reportarError("tipo de dato no valido")
        }
        return null
    }

    /**
     * <TipoRetorno> ::= int | decimal | string | char | bool
     */
     private fun esTipoRetorno(): Token?{

             if(tokenActual.categoria == Categoria.PALABRA_RESERVADA && tokenActual.lexema == "Int" || tokenActual.lexema == "Decimal"||tokenActual.lexema == "String" ||tokenActual.lexema == "Char" ||tokenActual.lexema == "Bool" || tokenActual.lexema == "void" ){
                      return  tokenActual
             } else {
                 reportarError("tipo de retorno no valido")
             }
        return null
    }

    /**
     * <BloqueSentencias> ::= <Sentencia>["{"<ListaSentencias>"}"]
     */
     private fun esBloqueSentencias(): ArrayList<Sentencia>?{
        val posInicial = posicionActual
        if(tokenActual.categoria == Categoria.LLAVE_IZQUIERDA){
                      obtenerSiguienteToken()
                      var listaSentencias = esListaSentencias()
                      if(tokenActual.categoria == Categoria.LLAVE_DERECHA){
                          return listaSentencias
                      } else {
                          reportarError("Falta la llave derecha del bloque de sentencias")
                      }
        } else {
            reportarError("Falta la llave izquierda del bloque de sentencias")
        }
        back(posInicial)
        return null
    }

    /**
     * <ListaSentencias> ::= <Sentencia>[<ListaSentencias>]
     */
    private fun esListaSentencias(): ArrayList<Sentencia>{
        val listaSentencias = ArrayList<Sentencia>()
        var sentencia = esSentencia()

        while (sentencia != null){
              listaSentencias.add(sentencia)
              sentencia = esSentencia()
        }
        return listaSentencias
    }


    /**
     * <Sentencia> ::= <Arreglo> | <Decision> | <Ciclo> | <Asignacion>
     */
     private fun esSentencia(): Sentencia?{
           var sentencia: Sentencia? = esArreglo()

           if(sentencia!=null){
               return sentencia
           }
            sentencia = esLecturaDatos()
              if(sentencia!=null){
                 return sentencia
           }
            sentencia = esImpresionDatos()
            if(sentencia!=null){
               return sentencia
            }
           sentencia = esDesicion()
           if(sentencia!=null){
               return sentencia
           }
           sentencia = esCiclo()
           if(sentencia != null){
               return sentencia
           }
           sentencia = esDeclaracionVariable()
           if (sentencia != null){
               return sentencia
           }
        /**
         * sentencia = esInvocarFuncion()
              if (sentencia != null){
                return sentencia
               }
         */

           sentencia = esAsignacion()
           println("sentencia Asignacion "+sentencia)
           if(sentencia!= null){
               return sentencia
           }
           sentencia = esReturn()
           if(sentencia!= null){
            return sentencia
           }
        return null
     }

    /**
     * <InvocacionFuncion> ::= nombreFuncion "("[<ListaArgumentos>]")" ";"
     */
    private fun esInvocarFuncion(): InvocacionFuncion?{
          println("token que llega invocar "+tokenActual)
          val pos = posicionActual
          if(tokenActual.categoria == Categoria.IDENTIFICADOR){
                var nombre = tokenActual
                obtenerSiguienteToken()
                if(tokenActual.categoria == Categoria.PARENTESIS_IZQUIERDO){
                    obtenerSiguienteToken()
                    var parametros = esListaArgumentos()
                    if(tokenActual.categoria == Categoria.PARENTESIS_DERECHO){
                        obtenerSiguienteToken()
                        return InvocacionFuncion(nombre, parametros)
                    }
                }

          }
        back(pos)
        return null
    }

    /**
     * <Return> ::= "return" <Retorno> ";"
     */

    private fun esReturn(): Retorno?{
        if(tokenActual.categoria == Categoria.PALABRA_RESERVADA && tokenActual.lexema == "return"){
             val token = tokenActual
             obtenerSiguienteToken()
             var retorno = esExpresion()
             if(retorno!=null){
                 if(tokenActual.categoria == Categoria.FIN_SENTENCIA){
                     obtenerSiguienteToken()
                     return Retorno(token,retorno)
                 }
             }

        }
       return null
    }


    /**
     * <Arreglo> ::=  identificador "[*]" <TipoDato> [   "=" "("<ListaArgumentos>")"  ]  ";"
     */

    private fun esArreglo (): Arreglo?{
          val pos = posicionActual
          if(tokenActual.categoria == Categoria.IDENTIFICADOR){
              val nombre = tokenActual
              obtenerSiguienteToken()
              if(tokenActual.categoria  == Categoria.PALABRA_RESERVADA && tokenActual.lexema == "[*]"){
                  obtenerSiguienteToken()
                  val tipoDato = esTipoDato()
                  if(tipoDato!=null){
                      obtenerSiguienteToken()
                      var listaArgumentos = ArrayList<Expresion>()
                      if(tokenActual.categoria == Categoria.OPERADOR_ASIGNACION && tokenActual.lexema == "="){
                          obtenerSiguienteToken()
                          if(tokenActual.categoria == Categoria.PARENTESIS_IZQUIERDO){
                              obtenerSiguienteToken()
                              listaArgumentos = esListaArgumentos()
                              if(tokenActual.categoria == Categoria.PARENTESIS_DERECHO){
                                  obtenerSiguienteToken()
                                  if(tokenActual.categoria == Categoria.FIN_SENTENCIA){
                                      obtenerSiguienteToken()
                                      return Arreglo(nombre,tipoDato,listaArgumentos)
                                  }
                              }
                          }
                      }
                      if(tokenActual.categoria == Categoria.FIN_SENTENCIA){
                            obtenerSiguienteToken()
                            return Arreglo(nombre,tipoDato,listaArgumentos)
                      }
                  }
              }
          }
          back(pos)
          return null
    }


    /**
     * <ListaArgumentos> ::= <Expresion>[","<ListaArgumentos>]
     */

    private fun esListaArgumentos():ArrayList<Expresion>{
        var listaArgumentos = ArrayList<Expresion>()
            var expresion = esExpresion()
            while (expresion != null) {
                listaArgumentos.add(expresion)
                if (tokenActual.categoria == Categoria.COMA) {
                    obtenerSiguienteToken()
                    expresion = esExpresion()
                } else {
                    if(tokenActual.categoria !== Categoria.PARENTESIS_DERECHO) {
                        reportarError("Falta la coma en los datos del arreglo")
                    }
                    break
                }
            }
        return listaArgumentos
    }


    /**
     * <DeclaracionVariable> ::= identificador ":" <TipoDato> ";"
     */
    private fun esDeclaracionVariable(): DeclaracionVariable?{
        val posIncial = posicionActual
        if(tokenActual.categoria == Categoria.IDENTIFICADOR){
            var identificador = tokenActual
            obtenerSiguienteToken()
            if(tokenActual.categoria == Categoria.DOS_PUNTOS){
                obtenerSiguienteToken()
                var tipoDato = esTipoDato()
                if(tipoDato!=null){
                    var tipoDato = tokenActual
                    obtenerSiguienteToken()
                    if(tokenActual.categoria == Categoria.FIN_SENTENCIA){
                        obtenerSiguienteToken()
                        return DeclaracionVariable(identificador,tipoDato)
                    }
                    reportarError("Falta el fin de sentencia en la declaracion de la variable")

                } else {
                    reportarError("Falta el tipo de dato en la declaracion de la variable")
                }
            } else if(tokenActual.categoria != Categoria.OPERADOR_ASIGNACION) {
                reportarError("Falta el operador : en la declaracion de la variable")
            }
        }
        back(posIncial)
        return null
    }


    /**
     * <Desicion> ::= if <ExpresionLogica> : <Bloque> [else <Bloque>]
     */

    private fun esDesicion (): Desicion?{
        if(tokenActual.categoria == Categoria.PALABRA_RESERVADA && tokenActual.lexema == "if"){
             obtenerSiguienteToken()
             val expresion = esExpresionLogica()
             if(expresion != null){
                 if(tokenActual.lexema == ":"){
                      obtenerSiguienteToken()
                      val bloqueY = esBloqueSentencias()
                      if(bloqueY !=null){
                          obtenerSiguienteToken()
                          if(tokenActual.lexema == "else"){
                              obtenerSiguienteToken()
                              val bloqueN = esBloqueSentencias()
                              obtenerSiguienteToken()
                              if(bloqueN!=null){
                                  return Desicion(expresion,bloqueY,bloqueN)
                              }
                          } else {
                              return Desicion(expresion, bloqueY, null)
                          }
                      }
                 } else {
                     reportarError("Falta la palabra reservada :")
                 }
             } else {
                 reportarError("Error en la condicion logica")
             }
        }
        return null
    }


    /**
     * <Ciclo> ::= loop <ExpresionLogica> ":" <BloqueSentencias>"
     */

    private fun esCiclo (): Ciclo?{
        if(tokenActual.categoria ==Categoria.PALABRA_RESERVADA && tokenActual.lexema == "loop") {

            obtenerSiguienteToken()
            val expresion= esExpresionLogica()

            if(expresion!=null){
                if(tokenActual.lexema == ":"){
                    obtenerSiguienteToken()
                    val bloque = esBloqueSentencias()
                    obtenerSiguienteToken()
                    return Ciclo(expresion,bloque)
                }else{
                    reportarError("Faltan  : despues de la expresion logica del ciclo ")
                }
            }else{
                reportarError("Hay un error en la condicion")
            }
        }
        return null
    }

    /**
     * <Asignacion> ::= <identificador> <operadorAsignacion> <Expresion> | <InvocacionFuncion> ";"
     */
    private fun esAsignacion(): Asignacion?{
        //var variable = esVariable()
        println("en asignacion "+tokenActual)
        if(tokenActual.categoria == Categoria.IDENTIFICADOR){
              var identificador = tokenActual
              obtenerSiguienteToken()
              var operadorAsignacion = esOperadorAsignacion()
              if(operadorAsignacion!=null){
                  obtenerSiguienteToken()
                  var expresion = esExpresion()
                  println("que expresion retorna "+expresion)
                  if(expresion!=null){
                      if(tokenActual.categoria == Categoria.FIN_SENTENCIA){
                          obtenerSiguienteToken()
                          return Asignacion(identificador,operadorAsignacion,expresion)
                      }
                      reportarError("falta el fin de sentencia en la asignacion")
                  } else {
                      modoPanico()
                      reportarError("expresion para la asignacion no valida")
                  }
              } else{
                  reportarError("operador de asignacion no valido")
              }
        }
        return null
    }

    /**
     * <OperadorAsignacion> ::= "+=" | "-=" | "*=" | "/=" | "%=" | "="
     */

     private fun esOperadorAsignacion(): Token?{
           if(tokenActual.categoria == Categoria.OPERADOR_ASIGNACION && tokenActual.lexema== "+=" || tokenActual.lexema== "-=" || tokenActual.lexema== "*=" || tokenActual.lexema== "/=" || tokenActual.lexema== "%=" || tokenActual.lexema== "="){
                 return tokenActual
           }
        return null
    }

    /**
     * <Expresion> ::= <ExpresionLogica> | <ExpresionAritmetica>
     */

    private fun esExpresion(): Expresion?{
        var expresion: Expresion? = esExpresionLogica()
        if(expresion != null){
            return expresion
        }
        println("sige para expresion aritmetetica "+tokenActual.lexema)
        expresion = esExpresionAritmetica()
        if(expresion != null){
            return expresion
        }
        /**
         *  expresion = esVariable()
            if(expresion != null){
              return expresion
             }
         */
        expresion = esExpresionCadena()
        if(expresion != null){
            return esExpresion()
        }

        expresion = esInvocarFuncion()

        println("termina las expresiones "+expresion)
        println("token "+tokenActual)
        return expresion
    }

    /**
     * <ExpresionCadena> ::= cadena["+"<Expresion>]| identificador
     */

    private fun exExpresionCadena(): ExpresionCadena?{
         if(tokenActual.categoria == Categoria.CADENA){
             val cadena = tokenActual
             obtenerSiguienteToken()
             var expresion: Expresion? = null
             if(tokenActual.lexema == "+"){
                 obtenerSiguienteToken()
                 expresion = esExpresion()
             }
             return ExpresionCadena(cadena,expresion)
         }
        if(tokenActual.categoria == Categoria.IDENTIFICADOR){
            val variable = tokenActual
            obtenerSiguienteToken()
            return ExpresionCadena(variable)
        }
        return null
    }

    /**
     * <ExpresionLogica> ::= <ExpresionLogica> operadorLogicoBinario <ExpresionLogica> |  “(”<ExpresionLogica>“)” | operadorNegacion “(”<ExpresionLogica>”)” | <ExpresionRelacional>
     *
     *  es equivalente a:
     *
     * <ExpresionLogica> ::= “(”<ExpresionLogica>“)” [operadorLogicoBinario <ExpresionLogica>] | operadorNegacion “(”<ExpresionLogica>”)” [operadorLogicoBinario <ExpresionLogica>]
     * | “(“<ExpresionRelacional>”)” [operadorLogicoBinario <ExpresionLogica>]
     */
    private fun esExpresionLogica(): ExpresionLogica? {
        val posInicial = posicionActual
        println("que tenemos en expresion logica "+tokenActual.lexema)
        if (tokenActual.categoria == Categoria.PARENTESIS_IZQUIERDO) {
            obtenerSiguienteToken()

            val expresion = esExpresionLogica()

            if (expresion != null) {

                if (tokenActual.categoria == Categoria.PARENTESIS_DERECHO) {

                    obtenerSiguienteToken()

                    if (tokenActual.categoria == Categoria.OPERADOR_LOGICO && (tokenActual.lexema == "and" || tokenActual.lexema == "or")) {
                        val operador = tokenActual
                        obtenerSiguienteToken()

                        val expresion2 = esExpresionLogica()

                        if (expresion2 != null) {
                            return ExpresionLogica(expresion, operador, expresion2)
                        } else {
                            reportarError("Hace falta otra expresión lógica para el tipo de operador ingresado")
                        }
                    } else {
                        //reportarError("El operador ingresado no corresponde al tipo de evaluación que se quiere realizar")
                        if(tokenActual.categoria != Categoria.OPERADOR_ARITMETICO){
                            return expresion
                        }

                    }
                } else {
                    reportarError("Falta poner después de la expresión lógica el paréntesis derecho ) ")
                }
            } else {
                devolverse()
                reportarError("La expresión ingresada no es correcta")
            }
        } else if (tokenActual.categoria == Categoria.OPERADOR_LOGICO && tokenActual.lexema == "not") {

            val operadorNegacion = tokenActual
            obtenerSiguienteToken()

            if (tokenActual.categoria == Categoria.PARENTESIS_IZQUIERDO) {
                obtenerSiguienteToken()

                val expresion = esExpresionLogica()

                if (expresion != null) {

                    if (tokenActual.categoria == Categoria.PARENTESIS_DERECHO) {
                        obtenerSiguienteToken()

                        if (tokenActual.categoria == Categoria.OPERADOR_LOGICO) {

                            if (tokenActual.lexema == "and" || tokenActual.lexema == "or") {
                                val operador = tokenActual
                                obtenerSiguienteToken()

                                val expresion2 = esExpresionLogica()
                                if (expresion2 != null) {
                                    return ExpresionLogica(operadorNegacion, expresion, operador, expresion2)
                                } else {
                                    reportarError("La expresion ingresada después del operador logico binario no es correcta ")
                                }
                            }else{
                                reportarError("El tipo de operador ingresado no es válido para la comparación planteada")
                            }
                        } else {
                            return ExpresionLogica(operadorNegacion, expresion)
                        }
                    } else {
                        reportarError("Falta poner después de la expresión lógica el paréntesis derecho )")
                    }
                } else {
                    reportarError("La expresión ingresada después del operador de negación no es correcta")
                }
            } else {
                reportarError("Después del operador de negación debe ir un paréntesis izquierdo ( ")
            }
        } else {

            val expresion = esValorLogico()
            if(expresion!=null){
                  if(tokenActual.categoria == Categoria.OPERADOR_LOGICO){
                       val operador = tokenActual
                       obtenerSiguienteToken()
                       val expresion2 = esExpresionLogica()
                       if(expresion2!=null){
                            return ExpresionLogica(ExpresionLogica(expresion),operador,expresion2)
                       }
                  }
                  if(tokenActual.categoria == Categoria.DOS_PUNTOS || tokenActual.categoria == Categoria.PARENTESIS_DERECHO
                      || tokenActual.categoria == Categoria.FIN_SENTENCIA){
                      return ExpresionLogica(expresion)
                  }
            }

        }
        back(posInicial)
        return null
    }

    /**
     * <ExpresionRelacional> ::= <ExpresionAritmetica> operadorRelacional <ExpresionAritmetica>
     */
    private fun esValorLogico(): ValorLogico? {
        val expresion = esExpresionAritmetica()
        println("en valor logico primera expresion aritmetica "+expresion)
        println("token actual "+tokenActual.lexema)
        if (expresion != null) {
            if (tokenActual.categoria == Categoria.OPERADOR_RELACIONAL) {
                val operador = tokenActual
                obtenerSiguienteToken()

                val expresion2 = esExpresionAritmetica()
                if (expresion2 != null) {
                    return ValorLogico(expresion, operador, expresion2)
                } else {
                    reportarError("La expresion ingresada después del operador no es correcta")
                }
            }
        } else if(tokenActual.categoria == Categoria.PALABRA_RESERVADA && (tokenActual.lexema == "R" || tokenActual.lexema=="F")) {
            val logico = tokenActual
            obtenerSiguienteToken()
            return ValorLogico(logico)
        }
        return null
    }


    /**
     * <ExpresionRelacional> ::= <ExpresionRelacional> operadorRelacional <ExpresionRelacional> | “(”<ExpresionRelacional>“)” | <ValorLogico>
     *
     *  es equivalente a:
     *  <ExpresionRelacional> ::= “(”<ExpresionRelacional>“)” [operadorRelacional <ExpresionRelacional>] | <ValorLogico> [operadorRelacional <ExpresionRelacional> ]
     */
    private fun esExpresionRelacional(): ExpresionRelacional? {
        println("como llega al operador relacional" + tokenActual.lexema)
        if (tokenActual.categoria == Categoria.PARENTESIS_IZQUIERDO) {
            obtenerSiguienteToken()

            val expresion = esExpresionRelacional()

            if (expresion != null) {

                if (tokenActual.categoria == Categoria.PARENTESIS_DERECHO) {

                    obtenerSiguienteToken()

                    if (tokenActual.categoria == Categoria.OPERADOR_RELACIONAL) {
                        val operador = tokenActual
                        obtenerSiguienteToken()

                        val expresion2 = esExpresionRelacional()

                        if (expresion2 != null) {
                            return ExpresionRelacional(expresion, operador, expresion2)
                        }else{
                            reportarError("La expresión ingresada después del operador relacional no es correcta")
                        }
                    } else {
                        return ExpresionRelacional(expresion)
                    }
                } else {
                    reportarError("Falta poner después de la expresión relacional el paréntesis derecho ) ")
                }
            } else {
                reportarError("La expresión ingresada no es correcta")
            }
        } else {

            val valor = esValorLogico()
            if (valor != null) {
                if (tokenActual.categoria == Categoria.OPERADOR_RELACIONAL) {
                    val operador = tokenActual
                    obtenerSiguienteToken()

                    val expresion = esExpresionRelacional()

                    if (expresion != null) {
                        return ExpresionRelacional(valor, operador, expresion)
                    }else{
                        reportarError("La expresión ingresada después del operador no es correcta")
                    }
                } else {
                    return ExpresionRelacional(valor)
                }
            } else {
                reportarError("La expresión ingresada antes del operador relacional no es correcta")
            }
        }
        return null
    }

    /**
     * <ImpresionDatos> ::= log "(" <Dato> ")" ";"
     */
    private fun esImpresionDatos(): Impresion? {
        if (tokenActual.categoria == Categoria.PALABRA_RESERVADA && tokenActual.lexema == "log") {
            obtenerSiguienteToken()
            if (tokenActual.categoria == Categoria.PARENTESIS_IZQUIERDO) {
                obtenerSiguienteToken()
                var dato = esDato()
                if (dato != null) {
                    obtenerSiguienteToken()
                    if (tokenActual.categoria == Categoria.PARENTESIS_DERECHO) {

                        obtenerSiguienteToken()

                        if (tokenActual.categoria == Categoria.FIN_SENTENCIA) {
                            obtenerSiguienteToken()
                            return Impresion(dato)

                        } else {
                            reportarError("Falta ; para finalizar la impresion de datos")
                        }
                    } else {
                        reportarError("Falta el parentesis derecho en la impresion ")
                    }
                } else{
                    reportarError("debes colocar algo para imprimir")
                }
            } else {
                reportarError("Falta el parentesis izquierdo en la impresion ")
            }
        }
        return null
    }

    /**
     * <IngresoDatos> ::= <identificador> <operadorAsignacion> input (<Dato>)“;”
     */

    private fun esLecturaDatos(): Lectura? {
        val pos = posicionActual
        if (tokenActual.categoria == Categoria.IDENTIFICADOR) {
            obtenerSiguienteToken()
            var operadorAsignacion = esOperadorAsignacion()
            if (operadorAsignacion != null) {
                obtenerSiguienteToken()
                if (tokenActual.categoria == Categoria.PALABRA_RESERVADA && tokenActual.lexema == "input") {
                    obtenerSiguienteToken()

                    if (tokenActual.categoria == Categoria.PARENTESIS_IZQUIERDO) {

                        obtenerSiguienteToken()

                        var dato = esDato()

                        if (dato != null) {

                            obtenerSiguienteToken()

                            if (tokenActual.categoria == Categoria.PARENTESIS_DERECHO) {

                                obtenerSiguienteToken()

                                if (tokenActual.categoria == Categoria.FIN_SENTENCIA) {
                                    obtenerSiguienteToken()
                                    return Lectura(dato)

                                } else {
                                    reportarError("Falta ; para finalizar la entrada de datos")
                                }
                            } else {
                                reportarError("Falta el parentesis derecho")
                            }
                        } else {

                            reportarError("No se ingreso ningun dato para leer")
                        }
                    }
                }
            }
        }
        back(pos)
        return null
    }

    /**
     *  <Dato> ::= NumeroReal | NumeroEntero | CadenaCaracteres
     */
    private fun esDato(): Token? {
        if (tokenActual.categoria == Categoria.CADENA || tokenActual.categoria == Categoria.CARACTER ||
            tokenActual.categoria == Categoria.ENTERO || tokenActual.categoria == Categoria.DECIMAL
        ) {
            return tokenActual
        }

        return null
    }

    /**
     * <ExpresionAritmetica> ::= <ExpresionAritmetica> operadorAritmetico <ExpresionAritmetica> | "("<ExpresionAritmetica>")" | <ValorNumerico>
     *
     *  es equivalente a:
     *
     *  <ExpresionAritmetica> ::=  "("<ExpresionAritmetica>")" [ operadorAritmetico <ExpresionAritmetica> ] | <ValorNumerico> [ operadorAritmetico <ExpresionAritmetica> ]
     */
    fun esExpresionAritmetica(): ExpresionAritmetica? {
        val pos = posicionActual
        println("que tenemos "+ tokenActual.lexema)

        if (tokenActual.categoria == Categoria.PARENTESIS_IZQUIERDO) {
            obtenerSiguienteToken()
            val expresion = esExpresionAritmetica()
            if (expresion != null) {
                if (tokenActual.categoria == Categoria.PARENTESIS_DERECHO) {
                    obtenerSiguienteToken()
                    if (tokenActual.categoria == Categoria.OPERADOR_ARITMETICO) {
                        val operador = tokenActual
                        obtenerSiguienteToken()
                        val expresion2 = esExpresionAritmetica()
                        if (expresion2 != null) {
                            return ExpresionAritmetica(expresion, operador, expresion2)
                        }
                    } else {
                        return ExpresionAritmetica(expresion)
                    }
                }else{
                    devolverse()
                    reportarError("Falta poner después de la expresión aritmética el paréntesis derecho ) ")
                }
            }else{
                devolverse()
                reportarError("La expresión ingresada no es correcta")
            }
        } else {


            val numerico = esValorNumerico()
            println("valor numerico "+numerico)

            if (numerico != null) {
                obtenerSiguienteToken()

                if (tokenActual.categoria == Categoria.OPERADOR_ARITMETICO) {
                    val operador = tokenActual
                    obtenerSiguienteToken()

                    val expresion = esExpresionAritmetica()

                    if (expresion != null) {
                        return ExpresionAritmetica(numerico, operador, expresion)
                    } else {
                        reportarError("la expresion aritmetica no es correcta")
                    }

                } else {
                        return ExpresionAritmetica(numerico)
                }
            }

        }
        back(pos)
        return null
    }

    /**
     * <ValorNumerico> ::= [<Signo>] decimal | [<Signo>] entero
     */

    private fun esValorNumerico(): ValorNumerico? {
        var signo: Token? = null

        if (tokenActual.categoria == Categoria.OPERADOR_ARITMETICO && (tokenActual.lexema == "+" || tokenActual.lexema == "-")) {
            signo = tokenActual
            obtenerSiguienteToken()
        }

        if ((tokenActual.categoria == Categoria.IDENTIFICADOR && listaTokens[posicionActual+1].categoria != Categoria.PARENTESIS_IZQUIERDO) || tokenActual.categoria == Categoria.ENTERO || tokenActual.categoria == Categoria.DECIMAL) {
            val valor = tokenActual
            return ValorNumerico(signo, valor)
        }
        return null
    }

    /**
     * <Variable> ::= identificador
     */
     private fun esVariable(): Variable? {
        println("que llega a es variable "+tokenActual)
        if(tokenActual.categoria == Categoria.IDENTIFICADOR){
            val token = tokenActual
            obtenerSiguienteToken()
            return Variable(token)
        }
        return null
    }

    /**
     * <ExpresionCadena> ::= cadena ["+" <Expresion>] | identificador
     */
    private fun esExpresionCadena(): ExpresionCadena?{

         if(tokenActual.categoria == Categoria.CADENA){
             val cadena = tokenActual
             obtenerSiguienteToken()
             var expresion: Expresion? = null
             if(tokenActual.lexema == "+"){
                 obtenerSiguienteToken()
                 expresion = esExpresion()
             }
             return ExpresionCadena(cadena,expresion)
         }
         if(tokenActual.categoria == Categoria.IDENTIFICADOR && listaTokens[posicionActual+1].categoria != Categoria.PARENTESIS_IZQUIERDO){
             val variable = tokenActual
             obtenerSiguienteToken()
             return ExpresionCadena(variable)
         }
        return null
    }


}

