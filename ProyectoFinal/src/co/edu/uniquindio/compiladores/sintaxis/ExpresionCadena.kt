package co.edu.uniquindio.compiladores.sintaxis

import co.edu.uniquindio.compiladores.lexico.Error
import co.edu.uniquindio.compiladores.lexico.Token
import co.edu.uniquindio.compiladores.semantica.TablaSimbolos
import javafx.scene.control.TreeItem


class ExpresionCadena(): Expresion() {

    var cadena: Token? = null
    var expresion: Expresion? = null
    var identificador: Token? = null

    constructor(cadena:Token, expresion: Expresion?):this(){
         this.cadena = cadena
         this.expresion = expresion
    }

    constructor(identificador:Token):this(){
        this.identificador = identificador
    }

    override fun getArbolVisual(): TreeItem<String> {
        val raiz = TreeItem("Expresion cadena")
        if(identificador!=null){
            raiz.children.add(TreeItem(identificador!!.lexema))
        } else {
            raiz.children.add(TreeItem(cadena!!.lexema))
            if(expresion!=null){
                raiz.children.add(expresion!!.getArbolVisual())
            }
        }
        return raiz
    }

    override fun getTipo(tablaSimbolos: TablaSimbolos, listaErrores: ArrayList<Error>, ambito:String): String {
          return "String"
    }

    override fun analizarSemantica(tablaSimbolos: TablaSimbolos, listaErrores: ArrayList<Error>, ambito: String) {
        if(expresion!=null){
            expresion!!.analizarSemantica(tablaSimbolos,listaErrores,ambito)
        }
    }

    /**
     * <ExpresionCadena> ::= cadena ["+" <Expresion>] | identificador
     */
    override fun getJavaCode(): String {
        var codigo = cadena!!.getJavaCode()
        if (identificador != null) {

            //TODO hacer lo del identificador

        } else{
            if (expresion != null) {
                codigo += "+" + expresion!!.getJavaCode()
            }
        }
        return codigo
    }

}