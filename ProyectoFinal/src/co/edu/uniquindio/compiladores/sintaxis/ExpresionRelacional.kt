package co.edu.uniquindio.compiladores.sintaxis

import co.edu.uniquindio.compiladores.lexico.Error
import co.edu.uniquindio.compiladores.lexico.Token
import co.edu.uniquindio.compiladores.semantica.TablaSimbolos
import javafx.scene.control.TreeItem

class ExpresionRelacional(): Expresion() {

    var expresion1: ExpresionRelacional? = null
    var expresion2: ExpresionRelacional? = null
    var operador: Token? = null
    var valorLogico: ValorLogico? = null

    override fun getArbolVisual(): TreeItem<String> {
        var raiz = TreeItem("Expresion Relacional")
        if(expresion1!=null) raiz.children.add(expresion1!!.getArbolVisual())
        if(operador!=null) raiz.children.add(TreeItem("Operador: ${operador!!.lexema}"))
        if(expresion2!=null) raiz.children.add(expresion2!!.getArbolVisual())
        if(valorLogico!=null) raiz.children.add(valorLogico!!.getArbolVisual())
        return raiz
    }


    constructor(expresion1: ExpresionRelacional?, operador: Token?, expresion2: ExpresionRelacional?) : this() {
        this.expresion1 = expresion1
        this.operador = operador
        this.expresion2 = expresion2
    }

    constructor(valorLogico: ValorLogico,operador: Token?,expresion1: ExpresionRelacional?) : this() {
        this.valorLogico = valorLogico
        this.operador = operador
        this.expresion1 = expresion1
    }

    constructor(expresion1: ExpresionRelacional?) : this() {
        this.expresion1 = expresion1
    }

    constructor(valorLogico: ValorLogico?) : this() {
        this.valorLogico = valorLogico
    }

    override fun getTipo(tablaSimbolos: TablaSimbolos, listaErrores: ArrayList<Error>, ambito:String): String {
        return "Bool"
    }


}