package co.edu.uniquindio.compiladores.sintaxis

import co.edu.uniquindio.compiladores.lexico.Token
import javafx.scene.control.TreeItem

class Lectura(var dato: Token): Sentencia() {
    override fun toString(): String {
        return "Lectura(Dato=$dato)"
    }

    override fun getArbolVisual(): TreeItem<String> {
        var raiz = TreeItem("Lectura de datos")
        var variableRaiz = TreeItem(dato.lexema)
        raiz.children.add(variableRaiz)
        return raiz
    }

    override fun getJavaCode():String {
        var nombreVariable =""
        //TODO falta el analisis semantico
        when(dato.lexema){
            "entero" ->{
                // return  nombreVariable.getJavaCode()+"= Integer.parseInt(JOptionPane.showInputDialog(null,\"escribir:\") );"
            }
        }
        return "" ;

    }


}