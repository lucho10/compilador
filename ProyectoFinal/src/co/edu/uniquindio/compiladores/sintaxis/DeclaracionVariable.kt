package co.edu.uniquindio.compiladores.sintaxis

import co.edu.uniquindio.compiladores.lexico.Error
import co.edu.uniquindio.compiladores.lexico.Token
import co.edu.uniquindio.compiladores.semantica.TablaSimbolos
import javafx.scene.control.TreeItem

class DeclaracionVariable(var nombreVariable: Token, var tipoDato: Token): Sentencia() {

    override fun toString(): String {
        return "Variable(nombreVariable=$nombreVariable, tipoDato=$tipoDato)"
    }

    override fun getArbolVisual(): TreeItem<String> {
        var raiz = TreeItem("Declaracion Variable")
        var variableRaiz =  TreeItem("${nombreVariable.lexema} : ${tipoDato.lexema}")
        raiz.children.add(variableRaiz)
        return raiz
    }

    override fun llenarTablaSimbolos(tablaSimbolos: TablaSimbolos, listaErrores: ArrayList<Error>, ambito: String) {
        tablaSimbolos.guardarSimboloValor(nombreVariable.lexema,tipoDato.lexema,true,ambito,nombreVariable.fila,nombreVariable.columna)
    }
    override fun getJavaCode(): String {
        //TODO considerando que no se pueden declarar multiples variables en una linea
        return  tipoDato.getJavaCode() +" "+ nombreVariable.getJavaCode() +";"
    }
}