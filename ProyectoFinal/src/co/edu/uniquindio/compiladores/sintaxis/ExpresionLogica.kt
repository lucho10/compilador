package co.edu.uniquindio.compiladores.sintaxis

import co.edu.uniquindio.compiladores.lexico.Error
import co.edu.uniquindio.compiladores.lexico.Token
import co.edu.uniquindio.compiladores.semantica.TablaSimbolos
import javafx.scene.control.TreeItem

class ExpresionLogica(): Expresion() {

    var expresion1: ExpresionLogica? = null
    var expresion2: ExpresionLogica? = null
    var operador: Token? = null
    var operadorNegacion: Token? = null
    var expresionR: ValorLogico? = null

    override fun getArbolVisual(): TreeItem<String> {
        var raiz = TreeItem("Expresion Logica")
        if (operadorNegacion!=null) raiz.children.add(TreeItem("Operador: ${operadorNegacion!!.lexema}"))
        if(expresion1!=null) raiz.children.add(expresion1!!.getArbolVisual())
        if(operador!=null) raiz.children.add(TreeItem("Operador: ${operador!!.lexema}"))
        if(expresion2!=null) raiz.children.add(expresion2!!.getArbolVisual())
        if(expresionR!=null) raiz.children.add(expresionR!!.getArbolVisual())
        return raiz
    }

    constructor(expresion1: ExpresionLogica?, operador: Token?, expresion2: ExpresionLogica?) : this () {
        this.expresion1 = expresion1
        this.operador = operador
        this.expresion2 = expresion2
    }


    constructor(operadorNegacion: Token?,expresion1: ExpresionLogica?, operador: Token?, expresion2: ExpresionLogica?) : this () {
        this.operadorNegacion = operadorNegacion
        this.expresion1 = expresion1
        this.operador = operador
        this.expresion2 = expresion2
    }

    constructor(operadorNegacion: Token?, expresion1: ExpresionLogica?) : this () {
        this.operadorNegacion = operadorNegacion
        this.expresion1 = expresion1
    }

    constructor(expresionR: ValorLogico) : this () {
        this.expresionR = expresionR
    }

    constructor(expresionLogica: ExpresionLogica) : this () {
        this.expresion1 = expresionLogica
    }

    override fun getTipo(tablaSimbolos: TablaSimbolos, listaErrores: ArrayList<Error>,ambito:String): String {
        return "Bool"
    }

    override fun analizarSemantica(tablaSimbolos: TablaSimbolos, listaErrores: ArrayList<Error>, ambito: String) {

        if(expresion1 !=null){
             expresion1!!.analizarSemantica(tablaSimbolos,listaErrores,ambito)
        }
        if(expresion2 !=null){
            expresion2!!.analizarSemantica(tablaSimbolos,listaErrores,ambito)
        }
        if(expresionR !=null){
            expresionR!!.analizarSemantica(tablaSimbolos,listaErrores,ambito)
        }
    }

    /**
     * <ExpresionLogica> ::= <ExpresionLogica> operadorLogicoBinario <ExpresionLogica> |  “(”<ExpresionLogica>“)” | operadorNegacion “(”<ExpresionLogica>”)” | <ExpresionRelacional>
     *
     *  es equivalente a:
     *
     * <ExpresionLogica> ::= “(”<ExpresionLogica>“)” [operadorLogicoBinario <ExpresionLogica>] | operadorNegacion “(”<ExpresionLogica>”)” [operadorLogicoBinario <ExpresionLogica>]
     * | “(“<ExpresionRelacional>”)” [operadorLogicoBinario <ExpresionLogica>]
     */
    override fun getJavaCode(): String {

        //TODO revisar lo del valor logico por que no veo el operador binario
        if(expresion1!= null && expresion2!=null && operadorNegacion!=null && operador!=null){
            return operadorNegacion!!.getJavaCode()+expresion1!!.getJavaCode()+operador!!.getJavaCode()+expresion2!!.getJavaCode()
        }
        else if (expresion1!= null && expresion2!=null && operador!=null && expresionR==null && operadorNegacion==null){
            return expresion1!!.getJavaCode()+operador!!.getJavaCode() +expresion2!!.getJavaCode()
        }
        else if(expresion1== null && expresion2!=null && operador!=null && expresionR!=null && operadorNegacion!=null){
            return operadorNegacion!!.getJavaCode()+ expresion2!!.getJavaCode() + operador+expresionR!!.getJavaCode()
        }
        else if(expresion1!= null && expresion2==null && operador!=null && expresionR!=null && operadorNegacion!=null){

            return operadorNegacion!!.getJavaCode()+ expresion1!!.getJavaCode() + operador+expresionR!!.getJavaCode()
        }
        else if(expresion1!= null && expresion2==null && operador!=null && expresionR!=null && operadorNegacion==null){

            return expresion1!!.getJavaCode() + operador+expresionR!!.getJavaCode()
        }
        else if(expresion1== null && expresion2!=null && operador!=null && expresionR!=null && operadorNegacion==null){

            return expresion2!!.getJavaCode() +operador+expresionR!!.getJavaCode()
        }
        else if(expresion1== null && expresion2==null && operador==null && expresionR!=null && operadorNegacion!=null){

            return operadorNegacion!!.getJavaCode() + expresionR!!.getJavaCode()
        }

        return expresionR!!.getJavaCode()



    }




}