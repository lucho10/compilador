package co.edu.uniquindio.compiladores.sintaxis

import co.edu.uniquindio.compiladores.lexico.Error
import co.edu.uniquindio.compiladores.semantica.TablaSimbolos
import javafx.scene.control.TreeItem

class Desicion(var expresionLogica: ExpresionLogica, var listaSentencia: ArrayList<Sentencia>, var listaSentenciaElse: ArrayList<Sentencia>?): Sentencia() {
    override fun toString(): String {
        return "Desicion(expresionLogica=$expresionLogica, listaSentencia=$listaSentencia, listaSentenciaElse=$listaSentenciaElse)"
    }

    override fun getArbolVisual(): TreeItem<String>{
        var raiz  = TreeItem("Desicion")

        var condicion = TreeItem("Condicion")
        condicion.children.add(expresionLogica.getArbolVisual())

        raiz.children.add(condicion)

        var raizTrue = TreeItem("Sentencias Verdaderas")

        for (s in listaSentencia){
            raizTrue.children.add(s.getArbolVisual())
        }

        raiz.children.add(raizTrue)

        if(listaSentenciaElse!=null){
            var raizFalse = TreeItem("Sentencias Falsas")
            for (s in listaSentenciaElse!!){
                raizFalse.children.add(s.getArbolVisual())
            }
            raiz.children.add(raizFalse)
        }

         return raiz
    }

    override fun llenarTablaSimbolos(tablaSimbolos: TablaSimbolos, listaErrores: ArrayList<Error>, ambito: String) {
        for( s in listaSentencia ){
             s.llenarTablaSimbolos(tablaSimbolos,listaErrores,ambito)
        }
        if(listaSentenciaElse!=null){
            for( s in listaSentenciaElse!!){
                s.llenarTablaSimbolos(tablaSimbolos,listaErrores,ambito)
            }
        }
    }

    override fun analizarSemantica(tablaSimbolos: TablaSimbolos, listaErrores: ArrayList<Error>, ambito: String) {
        if(expresionLogica!=null){
            expresionLogica!!.analizarSemantica(tablaSimbolos,listaErrores,ambito)
        }
        for( s in listaSentencia ){
            s.analizarSemantica(tablaSimbolos,listaErrores,ambito)
        }
        if(listaSentenciaElse!=null){
            for( s in listaSentenciaElse!!){
                s.analizarSemantica(tablaSimbolos,listaErrores,ambito)
            }
        }
    }

}