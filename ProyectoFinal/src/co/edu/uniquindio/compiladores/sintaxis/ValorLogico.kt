package co.edu.uniquindio.compiladores.sintaxis

import co.edu.uniquindio.compiladores.lexico.Error
import co.edu.uniquindio.compiladores.lexico.Token
import co.edu.uniquindio.compiladores.semantica.TablaSimbolos
import javafx.scene.control.TreeItem

class ValorLogico(): Expresion(){

    var expresion1: ExpresionAritmetica? = null
    var expresion2: ExpresionAritmetica? = null
    var operador: Token? = null
    var logico: Token? = null

    constructor(expresion1: ExpresionAritmetica?,operador: Token?,expresion2: ExpresionAritmetica?):this(){
            this.expresion1 =expresion1
            this.operador = operador
            this.expresion2 = expresion2
    }

    constructor(logico: Token):this(){
          this.logico = logico
    }


    override fun toString(): String {
        return "ValorLogico(expresion1=$expresion1, operador=$operador, expresion2=$expresion2)"
    }
    override fun getArbolVisual(): TreeItem<String> {
        var raiz = TreeItem("Expresion Relacional")
        if(expresion1!=null) raiz.children.add(expresion1!!.getArbolVisual())
        if(operador!=null) raiz.children.add(TreeItem("Operador: ${operador!!.lexema}"))
        if(expresion2!=null) raiz.children.add(expresion2!!.getArbolVisual())
        if(logico!=null) raiz.children.add(TreeItem(logico!!.lexema))
        return raiz
    }

    override fun analizarSemantica(tablaSimbolos: TablaSimbolos, listaErrores: ArrayList<Error>, ambito: String) {
        if(expresion1!=null && expresion2!=null){
            expresion1!!.analizarSemantica(tablaSimbolos,listaErrores,ambito)
            expresion2!!.analizarSemantica(tablaSimbolos,listaErrores,ambito)
        }
    }

    override fun getTipo(tablaSimbolos: TablaSimbolos, listaErrores: ArrayList<Error>, ambito: String): String {
        return "Bool"
    }

    override fun getJavaCode(): String {
        if(expresion1!=null && expresion2!=null){
            return expresion1!!.getJavaCode()+" "+ operador!!.getJavaCode()+" "+ expresion2!!.getJavaCode()
        }
        if(logico!=null){
            return logico!!.getJavaCode()
        }
        return ""
    }
}